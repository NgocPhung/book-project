﻿using System.Collections.Generic;
using System.Linq;

namespace BookManagement.Models.Models
{
    public class PageList<TEntity> : IPageList
    {
        public int PageSize { get; set; }
        public int PageNumber { get; set; }
        public int TotalItem { get; set; }
        public int TotalPage { get; set; }
        
        public IEnumerable<TEntity> Data { get; set; }
        public int Skip => PageSize * (PageNumber - 1);
        
        public PageList(IEnumerable<TEntity> rows, int pageSize, int pageNumber)
        {
            PageSize = pageSize;
            PageNumber = pageNumber;
            TotalItem = rows.Count();
            TotalPage = TotalItem % PageSize > 0 ? TotalItem / PageSize + 1 : TotalItem / PageSize;
            Data = rows.Skip(this.Skip).Take(PageSize);
        }
    }
}