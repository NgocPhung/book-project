﻿using System;

namespace BookManagement.Models.Models
{
    public class Cost
    {
        public Guid CostId { get; set; }
        public double CostMoney { get; set; }
        public string Content { get; set; }
        public DateTime CreatedDate { get; set; }
        public Guid BookId { get; set; }

        public Cost(Guid costId, double costMoney, string content, DateTime createdDate, Guid bookId)
        {
            CostId = costId;
            CostMoney = costMoney;
            Content = content;
            CreatedDate = createdDate;
            BookId = bookId;
        }
    }
}