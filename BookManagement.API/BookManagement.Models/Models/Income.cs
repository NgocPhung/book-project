﻿using System;

namespace BookManagement.Models.Models
{
    public class Income
    {
        public Guid IncomeId { get; set; }
        public double IncometMoney { get; set; }
        public string Content { get; set; }
        public DateTime CreatedDate { get; set; }
        public Guid UserId { get; set; }

        public Income(Guid incomeId, double incometMoney, string content, DateTime createdDate, Guid userId)
        {
            IncomeId = incomeId;
            IncometMoney = incometMoney;
            Content = content;
            CreatedDate = createdDate;
            UserId = userId;
        }
    }
}