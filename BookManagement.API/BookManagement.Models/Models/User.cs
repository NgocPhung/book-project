﻿using System;

namespace BookManagement.Models.Models
{
    public class User
    {
        public Guid UserId { get; set; }
        public string Email { get; set; }
        public string PassWord { get; set; }
        public string Role { get; set; }

        //Ham dung khong bat buoc truyen tham so
        public User()
        {
            
        }
        
        //Ham dung BAT BUOC TRUYEN THAM SO VAO
        public User(Guid userId, string email, string passWord, string role)
        {
            UserId = userId;
            Email = email;
            PassWord = passWord;
            Role = role;
        }
    }
}