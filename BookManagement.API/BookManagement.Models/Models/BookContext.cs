﻿using Microsoft.EntityFrameworkCore;

namespace BookManagement.Models.Models
{
    public class BookContext : DbContext
    {
        protected BookContext()
        {
        }

        public BookContext(DbContextOptions options) : base(options)
        {
        }

        public DbSet<Request> Requests { get; set; }
        public DbSet<Fund> Funds { get; set; }
        
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            base.OnConfiguring(optionsBuilder);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }
    }
}