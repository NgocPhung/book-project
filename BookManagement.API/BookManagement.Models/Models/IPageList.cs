﻿namespace BookManagement.Models.Models
{
    public interface IPageList
    {
        int PageSize { get; set; }
        int PageNumber { get; set; }
        int TotalItem { get; set; }
    }
}