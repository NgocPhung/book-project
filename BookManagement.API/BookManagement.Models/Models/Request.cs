﻿using System;

namespace BookManagement.Models.Models
{
    public class Request
    {
        public Guid RequestId { get; set; }
        public string Content { get; set; }
        public Guid MemberId { get; set; }
        public Guid BookId { get; set; }
        public Guid AdminId { get; set; }
        public bool IsApprove { get; set; }

        public Request(Guid requestId, string content, Guid memberId, Guid bookId, Guid adminId)
        {
            RequestId = requestId;
            Content = content;
            MemberId = memberId;
            BookId = bookId;
            AdminId = adminId;
        }
    }
}