﻿using System;

namespace BookManagement.Models.Models
{
    public class Funds
    {
        public Guid FundsId { get; set; }
        public double Total { get; set; }
        public double Cost { get; set; }
        public double Income { get; set; }

        public Funds(Guid fundsId, double cost, double income)
        {
            FundsId = fundsId;
            Cost = cost;
            Income = income;
            Total = income - cost;
        }
    }
}