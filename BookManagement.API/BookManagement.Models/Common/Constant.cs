﻿namespace BookManagement.Models.Common
{
    public class Constant
    {
        //Khai bao cac constant cua he thong
        public struct ROLE_NAME
        {
            public const string ADMIN = "Admin";
            public const string MEMBER = "Member";
        }

        public const string ADMIN_ID = "0FAF6551-F027-487D-82DF-61F3D254C38E";
    }
}