﻿using System;
using BookManagement.Models.Models;
using Microsoft.EntityFrameworkCore;

namespace BookManagement.Models.Data
{
    public class BookDbContext : DbContext
    {
        public BookDbContext(DbContextOptions options) : base(options)
        {
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            base.OnConfiguring(optionsBuilder);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //Request
            modelBuilder.Entity<Request>().HasData(
                new Request(Guid.NewGuid(), "Content1", Guid.NewGuid(), Guid.NewGuid(), Guid.NewGuid())
            );
            modelBuilder.Entity<Request>().HasData(
                new Request(Guid.NewGuid(), "Content2", Guid.NewGuid(), Guid.NewGuid(), Guid.NewGuid())
            );
            modelBuilder.Entity<Request>().HasData(
                new Request(Guid.NewGuid(), "Content3", Guid.NewGuid(), Guid.NewGuid(), Guid.NewGuid())
            );
            modelBuilder.Entity<Request>().HasData(
                new Request(Guid.NewGuid(), "Content4", Guid.NewGuid(), Guid.NewGuid(), Guid.NewGuid())
            );
            modelBuilder.Entity<Request>().HasData(
                new Request(Guid.NewGuid(), "Content5", Guid.NewGuid(), Guid.NewGuid(), Guid.NewGuid())
            );
            modelBuilder.Entity<Request>().HasData(
                new Request(Guid.NewGuid(), "Content6", Guid.NewGuid(), Guid.NewGuid(), Guid.NewGuid())
            );
            
            //Funds
            
            modelBuilder.Entity<Funds>().HasData(
                new Funds(Guid.NewGuid(), 12, 13)
            );
            modelBuilder.Entity<Funds>().HasData(
                new Funds(Guid.NewGuid(), 111, 11)
            );
            modelBuilder.Entity<Funds>().HasData(
                new Funds(Guid.NewGuid(),09, 28)
            );
            modelBuilder.Entity<Funds>().HasData(
                new Funds(Guid.NewGuid(), 222, 12)
            );
            
            //User
            modelBuilder.Entity<User>().HasData(
                new User(Guid.NewGuid(), "abc@gmail.com", "123456", "Admin")
            );
            modelBuilder.Entity<User>().HasData(
                new User(Guid.NewGuid(), "dtnp@gmail.com", "123456", "Member")
            );
            modelBuilder.Entity<User>().HasData(
                new User(Guid.NewGuid(), "mm@gmail.com", "123456", "Member")
            );
            
            //Cost
            modelBuilder.Entity<Cost>().HasData(
                new Cost(Guid.NewGuid(), 20000, "Mua sach A", DateTime.Now, Guid.NewGuid())
            );
            modelBuilder.Entity<Cost>().HasData(
                new Cost(Guid.NewGuid(), 10000, "Mua sach B", DateTime.Now, Guid.NewGuid())
            );
            modelBuilder.Entity<Cost>().HasData(
                new Cost(Guid.NewGuid(), 50000, "Mua sach C", DateTime.Now, Guid.NewGuid())
            );
            modelBuilder.Entity<Cost>().HasData(
                new Cost(Guid.NewGuid(), 60000, "Mua sach D", DateTime.Now, Guid.NewGuid())
            );
            
            //Income
            modelBuilder.Entity<Income>().HasData(
                new Income(Guid.NewGuid(), 200000, "A nop quy thang 12", DateTime.Now, Guid.NewGuid())
            );
            modelBuilder.Entity<Income>().HasData(
                new Income(Guid.NewGuid(), 100000, "B nop quy thang 12", DateTime.Now, Guid.NewGuid())
            );
            modelBuilder.Entity<Income>().HasData(
                new Income(Guid.NewGuid(), 150000, "C nop quy thang 12", DateTime.Now, Guid.NewGuid())
            );
            modelBuilder.Entity<Income>().HasData(
                new Income(Guid.NewGuid(), 160000, "D nop quy thang 12", DateTime.Now, Guid.NewGuid())
            );
        }

        public DbSet<Request> Requests { get; set; }
        
        public DbSet<Funds> Fundses { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Cost> Costs { get; set; }
        public DbSet<Income> Incomes { get; set; }
    }
}