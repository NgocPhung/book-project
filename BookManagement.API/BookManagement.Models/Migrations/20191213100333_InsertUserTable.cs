﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace BookManagement.Models.Migrations
{
    public partial class InsertUserTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Fundses",
                keyColumn: "FundsId",
                keyValue: new Guid("5a7c35b2-eea4-4957-96c2-706ff3d0c5c0"));

            migrationBuilder.DeleteData(
                table: "Fundses",
                keyColumn: "FundsId",
                keyValue: new Guid("7ed5f72c-138c-4cbf-b7ec-91c623dc8b9e"));

            migrationBuilder.DeleteData(
                table: "Fundses",
                keyColumn: "FundsId",
                keyValue: new Guid("85e50cd1-8c0c-432d-8eb4-b56636e18843"));

            migrationBuilder.DeleteData(
                table: "Fundses",
                keyColumn: "FundsId",
                keyValue: new Guid("cfced8d5-e03f-4dd3-bffa-25b511d40fe8"));

            migrationBuilder.DeleteData(
                table: "Requests",
                keyColumn: "RequestId",
                keyValue: new Guid("0866188e-7ab2-4d70-8cc9-8ce010298427"));

            migrationBuilder.DeleteData(
                table: "Requests",
                keyColumn: "RequestId",
                keyValue: new Guid("1cded10b-a187-4a59-83f8-4b4f05fc6245"));

            migrationBuilder.DeleteData(
                table: "Requests",
                keyColumn: "RequestId",
                keyValue: new Guid("217a3762-cf53-4fb3-bdc9-26fc31f4857e"));

            migrationBuilder.DeleteData(
                table: "Requests",
                keyColumn: "RequestId",
                keyValue: new Guid("7d9d5b4d-bb19-41ea-91b3-9f1f13bdc94b"));

            migrationBuilder.DeleteData(
                table: "Requests",
                keyColumn: "RequestId",
                keyValue: new Guid("c7185df8-3119-4300-893a-1354260ef8d9"));

            migrationBuilder.DeleteData(
                table: "Requests",
                keyColumn: "RequestId",
                keyValue: new Guid("d92ab63d-9acd-4227-b3be-d92a861b1cbf"));

            migrationBuilder.RenameColumn(
                name: "Type",
                table: "Users",
                newName: "Email");

            migrationBuilder.InsertData(
                table: "Fundses",
                columns: new[] { "FundsId", "Content", "Cost", "Income", "Total" },
                values: new object[,]
                {
                    { new Guid("50d951bc-d2b3-4a08-948b-5b612de620a3"), "a", 12.0, 13.0, 123.0 },
                    { new Guid("7f58fefd-8c6c-4386-8174-888321c2b8fa"), "b", 11.0, 11.0, 111.0 },
                    { new Guid("a470daa7-125b-445f-886f-8b3cfcb52dff"), "c", 9.0, 28.0, 1985.0 },
                    { new Guid("fdbbce0a-7204-4ae1-a30c-eb3f87f0b008"), "d", 12.0, 13.0, 222.0 }
                });

            migrationBuilder.InsertData(
                table: "Requests",
                columns: new[] { "RequestId", "AdminId", "BookId", "Content", "IsApprove", "MemberId" },
                values: new object[,]
                {
                    { new Guid("e295ff6f-9902-45e2-8b47-6dc6c5619b02"), new Guid("651ea34d-2e88-4220-8a72-099d983a6733"), new Guid("c91c6472-584c-41fb-90ad-1cb0c4995928"), "Content1", false, new Guid("94507887-5c51-4c52-823d-3883fffa4cfc") },
                    { new Guid("95ffda47-0874-4ad7-81ec-e87d99d9ff4f"), new Guid("6e7c0590-f243-405b-9f38-1cc15059311d"), new Guid("75158715-fc7b-4c19-b4b1-e1d7e7b503d3"), "Content2", false, new Guid("dcf9dcd7-363b-46af-816a-cdc8386eccd3") },
                    { new Guid("12c6bcae-682a-4c5a-adb9-ba81237ead9f"), new Guid("1e6730f6-5bdc-49e5-8b4e-c46502fbb16b"), new Guid("176f5e87-cf14-4d0d-9a5c-9e5ee049934f"), "Content3", false, new Guid("e12dca7e-d6bc-42a5-8564-71547527bf02") },
                    { new Guid("a020bdd0-57b4-4aff-8ee9-9de9de5fd66f"), new Guid("8942cea7-ccbb-47f5-a37c-74cc504330aa"), new Guid("8538e002-887d-4661-8796-09656f36b984"), "Content4", false, new Guid("24ffed13-a0f1-48ed-9f21-7c98dd252bd6") },
                    { new Guid("0df40072-b830-4bf8-a26a-d1bab2b7254b"), new Guid("69780012-88a7-4673-a216-d86ce6f3f695"), new Guid("f1e07559-df74-4d7d-ba12-70d3936f4d84"), "Content5", false, new Guid("b30d3178-8733-497b-9d3a-f0c060f1f7ea") },
                    { new Guid("3164af22-b76e-4b70-9ac8-5b927bf57fe0"), new Guid("73714595-c5eb-4759-9179-26b92ef670d6"), new Guid("f91e3558-12e8-4d12-91ba-f64dcc12b56c"), "Content6", false, new Guid("5625a928-6cb7-4a4e-acb7-7729fa3879a7") }
                });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "UserId", "Email", "PassWord", "Role" },
                values: new object[,]
                {
                    { new Guid("0faf6551-f027-487d-82df-61f3d254c38e"), "abc@gmail.com", "123456", "Admin" },
                    { new Guid("92e5d33b-e2ca-422d-a1cf-75b3feb4de26"), "dtnp@gmail.com", "123456", "Member" },
                    { new Guid("731b6859-2b6e-4f0c-b1d4-19e2bab8cf2e"), "mm@gmail.com", "123456", "Member" }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Fundses",
                keyColumn: "FundsId",
                keyValue: new Guid("50d951bc-d2b3-4a08-948b-5b612de620a3"));

            migrationBuilder.DeleteData(
                table: "Fundses",
                keyColumn: "FundsId",
                keyValue: new Guid("7f58fefd-8c6c-4386-8174-888321c2b8fa"));

            migrationBuilder.DeleteData(
                table: "Fundses",
                keyColumn: "FundsId",
                keyValue: new Guid("a470daa7-125b-445f-886f-8b3cfcb52dff"));

            migrationBuilder.DeleteData(
                table: "Fundses",
                keyColumn: "FundsId",
                keyValue: new Guid("fdbbce0a-7204-4ae1-a30c-eb3f87f0b008"));

            migrationBuilder.DeleteData(
                table: "Requests",
                keyColumn: "RequestId",
                keyValue: new Guid("0df40072-b830-4bf8-a26a-d1bab2b7254b"));

            migrationBuilder.DeleteData(
                table: "Requests",
                keyColumn: "RequestId",
                keyValue: new Guid("12c6bcae-682a-4c5a-adb9-ba81237ead9f"));

            migrationBuilder.DeleteData(
                table: "Requests",
                keyColumn: "RequestId",
                keyValue: new Guid("3164af22-b76e-4b70-9ac8-5b927bf57fe0"));

            migrationBuilder.DeleteData(
                table: "Requests",
                keyColumn: "RequestId",
                keyValue: new Guid("95ffda47-0874-4ad7-81ec-e87d99d9ff4f"));

            migrationBuilder.DeleteData(
                table: "Requests",
                keyColumn: "RequestId",
                keyValue: new Guid("a020bdd0-57b4-4aff-8ee9-9de9de5fd66f"));

            migrationBuilder.DeleteData(
                table: "Requests",
                keyColumn: "RequestId",
                keyValue: new Guid("e295ff6f-9902-45e2-8b47-6dc6c5619b02"));

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "UserId",
                keyValue: new Guid("0faf6551-f027-487d-82df-61f3d254c38e"));

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "UserId",
                keyValue: new Guid("731b6859-2b6e-4f0c-b1d4-19e2bab8cf2e"));

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "UserId",
                keyValue: new Guid("92e5d33b-e2ca-422d-a1cf-75b3feb4de26"));

            migrationBuilder.RenameColumn(
                name: "Email",
                table: "Users",
                newName: "Type");

            migrationBuilder.InsertData(
                table: "Fundses",
                columns: new[] { "FundsId", "Content", "Cost", "Income", "Total" },
                values: new object[,]
                {
                    { new Guid("cfced8d5-e03f-4dd3-bffa-25b511d40fe8"), "a", 12.0, 13.0, 123.0 },
                    { new Guid("7ed5f72c-138c-4cbf-b7ec-91c623dc8b9e"), "b", 11.0, 11.0, 111.0 },
                    { new Guid("5a7c35b2-eea4-4957-96c2-706ff3d0c5c0"), "c", 9.0, 28.0, 1985.0 },
                    { new Guid("85e50cd1-8c0c-432d-8eb4-b56636e18843"), "d", 12.0, 13.0, 222.0 }
                });

            migrationBuilder.InsertData(
                table: "Requests",
                columns: new[] { "RequestId", "AdminId", "BookId", "Content", "IsApprove", "MemberId" },
                values: new object[,]
                {
                    { new Guid("c7185df8-3119-4300-893a-1354260ef8d9"), new Guid("ea434506-3a83-437f-a4b8-924c13ed9bef"), new Guid("0fafd425-0327-4f34-b086-1e519be6b81a"), "Content1", false, new Guid("cd480f72-8e38-47ec-a6b7-b2aa13208250") },
                    { new Guid("217a3762-cf53-4fb3-bdc9-26fc31f4857e"), new Guid("a9eb6fcc-bcdf-46d8-ab84-bde990836420"), new Guid("4bc1f510-2506-4894-8bba-2fe243c1e67c"), "Content2", false, new Guid("41e22dc7-be86-4030-8c62-acc81afc7d57") },
                    { new Guid("7d9d5b4d-bb19-41ea-91b3-9f1f13bdc94b"), new Guid("1a28d58d-8a89-4f01-b244-82b435a348e7"), new Guid("cca80e6b-cbb5-4938-8e61-a368a5bcee5d"), "Content3", false, new Guid("ee2cb668-d762-4faf-bbc1-34ac22005dcf") },
                    { new Guid("1cded10b-a187-4a59-83f8-4b4f05fc6245"), new Guid("814e0293-5dfe-4b52-9fd4-eaa12595e415"), new Guid("92ffa226-987f-446f-80c6-a12524fbce2a"), "Content4", false, new Guid("11d26f77-0101-4b71-94fc-baa019072765") },
                    { new Guid("0866188e-7ab2-4d70-8cc9-8ce010298427"), new Guid("29ec2e80-67e6-4c87-91de-f065a17ae27e"), new Guid("e1abe466-3a3f-4c41-a5c5-449f190dbdf8"), "Content5", false, new Guid("19600866-0bb4-4707-af70-7a0f7afe0014") },
                    { new Guid("d92ab63d-9acd-4227-b3be-d92a861b1cbf"), new Guid("96085b46-5218-48f4-933a-02307b446583"), new Guid("705db47e-8b2b-49a0-878e-79af6d74a80e"), "Content6", false, new Guid("a0c71e54-24fc-4cbf-a9c2-a2b72de70551") }
                });
        }
    }
}
