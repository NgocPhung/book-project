﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace BookManagement.Models.Migrations
{
    public partial class AlterFundTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Fundses",
                keyColumn: "FundsId",
                keyValue: new Guid("a8f0b886-e313-4b64-9855-d36c7a0eb772"));

            migrationBuilder.DeleteData(
                table: "Fundses",
                keyColumn: "FundsId",
                keyValue: new Guid("b13d9a58-cb50-431c-94c4-612eae1fb364"));

            migrationBuilder.DeleteData(
                table: "Fundses",
                keyColumn: "FundsId",
                keyValue: new Guid("da7acd54-5f82-4263-89a4-b09d43897f07"));

            migrationBuilder.DeleteData(
                table: "Fundses",
                keyColumn: "FundsId",
                keyValue: new Guid("fbe24dae-59f6-4ce7-b297-b62ac0c2935c"));

            migrationBuilder.DeleteData(
                table: "Requests",
                keyColumn: "RequestId",
                keyValue: new Guid("51a14e90-0a41-4154-93f0-e88eebe43d91"));

            migrationBuilder.DeleteData(
                table: "Requests",
                keyColumn: "RequestId",
                keyValue: new Guid("5cefa264-2618-4ae9-a69f-fdb27bb08aa2"));

            migrationBuilder.DeleteData(
                table: "Requests",
                keyColumn: "RequestId",
                keyValue: new Guid("83dc877d-a30e-4459-bc3f-beb26818cfb5"));

            migrationBuilder.DeleteData(
                table: "Requests",
                keyColumn: "RequestId",
                keyValue: new Guid("83e915fd-b098-45d2-9660-9de57f1107b3"));

            migrationBuilder.DeleteData(
                table: "Requests",
                keyColumn: "RequestId",
                keyValue: new Guid("9dc1a25d-7c42-4e60-921b-bf6ec2be884e"));

            migrationBuilder.DeleteData(
                table: "Requests",
                keyColumn: "RequestId",
                keyValue: new Guid("bae62879-7821-495f-932e-ae22bcd575ea"));

            migrationBuilder.AddColumn<string>(
                name: "Content",
                table: "Fundses",
                nullable: true);

            migrationBuilder.InsertData(
                table: "Fundses",
                columns: new[] { "FundsId", "Content", "Cost", "Income", "Total" },
                values: new object[,]
                {
                    { new Guid("20fc6ae0-4bc4-4853-9182-0b4f7275b6c4"), "a", 12.0, 13.0, 123.0 },
                    { new Guid("52fcb781-437f-4750-a0b4-b05eda3df276"), "b", 11.0, 11.0, 111.0 },
                    { new Guid("ae8c7c98-3a10-4dde-bea4-77d8ba3f9e9b"), "c", 9.0, 28.0, 1985.0 },
                    { new Guid("9b98a78e-1bc9-482a-a77e-32782794bc07"), "d", 12.0, 13.0, 222.0 }
                });

            migrationBuilder.InsertData(
                table: "Requests",
                columns: new[] { "RequestId", "AdminId", "BookId", "Content", "IsApprove", "MemberId" },
                values: new object[,]
                {
                    { new Guid("76447fdb-d376-4d5e-8c27-2e998b37b6c2"), new Guid("f8de6205-27d6-4765-b033-4e4fddd85f48"), new Guid("5ac1598b-6f98-460f-902b-8c7e5fc5b1e8"), "Content1", false, new Guid("b3b6e977-2003-49c7-87f1-b4537c983bc7") },
                    { new Guid("5e160d91-4944-4212-8ae2-e127ab890908"), new Guid("625a4034-0ed0-4037-886c-ab099dc6e997"), new Guid("5e677880-6038-4c01-bd4d-0a06ae379226"), "Content2", false, new Guid("2f4c8dfd-01e3-4876-9cd4-e22b341e7f54") },
                    { new Guid("80d50406-a74d-4ab8-8428-857448cad775"), new Guid("df1e9db6-8cdd-4bd5-a327-89b2a6e21f73"), new Guid("bf1da96e-2f64-493c-b572-7d7fa9873aae"), "Content3", false, new Guid("939928b1-7a90-4688-807c-9262486cf0cd") },
                    { new Guid("17ee3be0-8568-4636-97f5-a99d3ef59270"), new Guid("3212ccd7-1e91-4fd7-a41b-eb0e54c4164b"), new Guid("e432e130-fefc-42ba-baf6-0852eaa1972c"), "Content4", false, new Guid("44a1ddbe-311a-4978-a4aa-4fbfc8200be6") },
                    { new Guid("21e2ee69-c9bb-4b92-b216-237cd07c1df3"), new Guid("aa2c5154-d081-4a8c-b30e-c6f93a8ece76"), new Guid("51705130-a18b-42f1-b64f-e19cf5407abe"), "Content5", false, new Guid("da09d16a-266e-4f02-8890-4fcdc8bed32c") },
                    { new Guid("6f9f961d-5357-49e8-82f9-d27bc7e28ab0"), new Guid("567573fe-08a1-464a-98c0-99f2da9ad5e7"), new Guid("345eb844-a509-4c9c-9edd-1a8146e9837e"), "Content6", false, new Guid("845335df-9bac-4ed6-9490-6edfab540736") }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Fundses",
                keyColumn: "FundsId",
                keyValue: new Guid("20fc6ae0-4bc4-4853-9182-0b4f7275b6c4"));

            migrationBuilder.DeleteData(
                table: "Fundses",
                keyColumn: "FundsId",
                keyValue: new Guid("52fcb781-437f-4750-a0b4-b05eda3df276"));

            migrationBuilder.DeleteData(
                table: "Fundses",
                keyColumn: "FundsId",
                keyValue: new Guid("9b98a78e-1bc9-482a-a77e-32782794bc07"));

            migrationBuilder.DeleteData(
                table: "Fundses",
                keyColumn: "FundsId",
                keyValue: new Guid("ae8c7c98-3a10-4dde-bea4-77d8ba3f9e9b"));

            migrationBuilder.DeleteData(
                table: "Requests",
                keyColumn: "RequestId",
                keyValue: new Guid("17ee3be0-8568-4636-97f5-a99d3ef59270"));

            migrationBuilder.DeleteData(
                table: "Requests",
                keyColumn: "RequestId",
                keyValue: new Guid("21e2ee69-c9bb-4b92-b216-237cd07c1df3"));

            migrationBuilder.DeleteData(
                table: "Requests",
                keyColumn: "RequestId",
                keyValue: new Guid("5e160d91-4944-4212-8ae2-e127ab890908"));

            migrationBuilder.DeleteData(
                table: "Requests",
                keyColumn: "RequestId",
                keyValue: new Guid("6f9f961d-5357-49e8-82f9-d27bc7e28ab0"));

            migrationBuilder.DeleteData(
                table: "Requests",
                keyColumn: "RequestId",
                keyValue: new Guid("76447fdb-d376-4d5e-8c27-2e998b37b6c2"));

            migrationBuilder.DeleteData(
                table: "Requests",
                keyColumn: "RequestId",
                keyValue: new Guid("80d50406-a74d-4ab8-8428-857448cad775"));

            migrationBuilder.DropColumn(
                name: "Content",
                table: "Fundses");

            migrationBuilder.InsertData(
                table: "Fundses",
                columns: new[] { "FundsId", "Cost", "Income", "Total" },
                values: new object[,]
                {
                    { new Guid("a8f0b886-e313-4b64-9855-d36c7a0eb772"), 12.0, 13.0, 123.0 },
                    { new Guid("fbe24dae-59f6-4ce7-b297-b62ac0c2935c"), 11.0, 11.0, 111.0 },
                    { new Guid("da7acd54-5f82-4263-89a4-b09d43897f07"), 9.0, 28.0, 1985.0 },
                    { new Guid("b13d9a58-cb50-431c-94c4-612eae1fb364"), 12.0, 13.0, 222.0 }
                });

            migrationBuilder.InsertData(
                table: "Requests",
                columns: new[] { "RequestId", "AdminId", "BookId", "Content", "IsApprove", "MemberId" },
                values: new object[,]
                {
                    { new Guid("83dc877d-a30e-4459-bc3f-beb26818cfb5"), new Guid("9682aaf5-bf44-4b27-a2c9-66ed17e63c13"), new Guid("4d9884a9-4017-44bf-9003-38c28b097a29"), "Content1", false, new Guid("b6f8141a-dd69-4a96-b8c9-34d9748ca39a") },
                    { new Guid("51a14e90-0a41-4154-93f0-e88eebe43d91"), new Guid("4d7728db-3166-47d1-a945-189a47c152bc"), new Guid("b765e5c2-a225-4797-a871-ce861addbb9b"), "Content2", false, new Guid("28785aa9-2a01-4681-b561-4536b94c6f56") },
                    { new Guid("bae62879-7821-495f-932e-ae22bcd575ea"), new Guid("23a97781-640b-434e-aa89-326a2c5f3e59"), new Guid("93bf23eb-73ec-4be6-9eb1-fbf94cd825fb"), "Content3", false, new Guid("0f1beee3-a6c5-482c-87bd-b80507addc9e") },
                    { new Guid("83e915fd-b098-45d2-9660-9de57f1107b3"), new Guid("e260774c-cfd4-48d4-9238-67b2f3271ebe"), new Guid("fa75df69-38b0-4687-8d00-e8b1b5984f0e"), "Content4", false, new Guid("29a5c873-b028-48d6-a263-441673e12839") },
                    { new Guid("9dc1a25d-7c42-4e60-921b-bf6ec2be884e"), new Guid("2daf87e3-9b99-4a41-921f-d9f2079ede06"), new Guid("6b2239c7-8e36-4276-b4fb-c20fd9c65463"), "Content5", false, new Guid("75992935-a077-4f13-b682-021532690134") },
                    { new Guid("5cefa264-2618-4ae9-a69f-fdb27bb08aa2"), new Guid("118fc55d-5b19-42e9-b107-d92c060daaef"), new Guid("e28e96a3-3146-48b8-a1ba-469e5a380cc2"), "Content6", false, new Guid("951873d3-56d7-4b57-96f9-23edffc027cc") }
                });
        }
    }
}
