﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace BookManagement.Models.Migrations
{
    public partial class InsertRequest : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Requests",
                columns: new[] { "RequestId", "AdminId", "BookId", "Content", "MemberId" },
                values: new object[,]
                {
                    { new Guid("8c53c8c2-1344-45cc-8f89-71f696833315"), new Guid("fe5d9d76-ddec-4746-93cb-ffac3bd1f1c6"), new Guid("a1b3b188-cd2d-466b-bf56-5799bda66a33"), "Content1", new Guid("b948f0c7-6475-4b8a-b2b8-c742847f7962") },
                    { new Guid("e3aec709-8eaf-47c1-8ae9-1d98d844e48f"), new Guid("2002afa2-ea0c-48af-9f18-19cefc75139c"), new Guid("95a72f70-5b50-40eb-a8e7-66494449eab1"), "Content2", new Guid("f5c22412-0885-44c2-a279-727b3d2f067c") },
                    { new Guid("1b3926c1-7835-42eb-af67-5cf0b03feac2"), new Guid("d5433a45-ed0b-4746-9639-95b02688f996"), new Guid("92f0d22a-0640-4099-91ee-ca9a1b35a804"), "Content3", new Guid("0f8d6f80-b593-4451-9f66-224e56b4e11d") },
                    { new Guid("cfd53626-aef9-4f71-8453-4fc7d3a73a46"), new Guid("64f0f211-12fd-43a2-9874-1d64840bed8c"), new Guid("2e1dd279-0e73-4324-92c8-022b90da94d6"), "Content4", new Guid("3f27b68f-d8e6-4f10-b9cf-6bfb9727c0ca") },
                    { new Guid("e112d579-e299-4c20-aa64-a21040a697c5"), new Guid("a553b5dd-f008-4a16-baa4-b564769f2257"), new Guid("4d60028a-6dda-4b0d-902a-3cdfaeebbb26"), "Content5", new Guid("a2c90c23-8e88-41ef-823b-e2a7deb91cb7") },
                    { new Guid("070a1ba2-9907-44da-babf-4a1cca145f46"), new Guid("e3c49fb2-fb8f-4a5e-95d1-ce26d81d15bc"), new Guid("cdb741c8-be15-4c36-9b06-fe19d51b939c"), "Content6", new Guid("1e91d276-f43c-4222-9979-633d502f4040") }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Requests",
                keyColumn: "RequestId",
                keyValue: new Guid("070a1ba2-9907-44da-babf-4a1cca145f46"));

            migrationBuilder.DeleteData(
                table: "Requests",
                keyColumn: "RequestId",
                keyValue: new Guid("1b3926c1-7835-42eb-af67-5cf0b03feac2"));

            migrationBuilder.DeleteData(
                table: "Requests",
                keyColumn: "RequestId",
                keyValue: new Guid("8c53c8c2-1344-45cc-8f89-71f696833315"));

            migrationBuilder.DeleteData(
                table: "Requests",
                keyColumn: "RequestId",
                keyValue: new Guid("cfd53626-aef9-4f71-8453-4fc7d3a73a46"));

            migrationBuilder.DeleteData(
                table: "Requests",
                keyColumn: "RequestId",
                keyValue: new Guid("e112d579-e299-4c20-aa64-a21040a697c5"));

            migrationBuilder.DeleteData(
                table: "Requests",
                keyColumn: "RequestId",
                keyValue: new Guid("e3aec709-8eaf-47c1-8ae9-1d98d844e48f"));
        }
    }
}
