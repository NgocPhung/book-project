﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace BookManagement.Models.Migrations
{
    public partial class CreateUserTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Fundses",
                keyColumn: "FundsId",
                keyValue: new Guid("20fc6ae0-4bc4-4853-9182-0b4f7275b6c4"));

            migrationBuilder.DeleteData(
                table: "Fundses",
                keyColumn: "FundsId",
                keyValue: new Guid("52fcb781-437f-4750-a0b4-b05eda3df276"));

            migrationBuilder.DeleteData(
                table: "Fundses",
                keyColumn: "FundsId",
                keyValue: new Guid("9b98a78e-1bc9-482a-a77e-32782794bc07"));

            migrationBuilder.DeleteData(
                table: "Fundses",
                keyColumn: "FundsId",
                keyValue: new Guid("ae8c7c98-3a10-4dde-bea4-77d8ba3f9e9b"));

            migrationBuilder.DeleteData(
                table: "Requests",
                keyColumn: "RequestId",
                keyValue: new Guid("17ee3be0-8568-4636-97f5-a99d3ef59270"));

            migrationBuilder.DeleteData(
                table: "Requests",
                keyColumn: "RequestId",
                keyValue: new Guid("21e2ee69-c9bb-4b92-b216-237cd07c1df3"));

            migrationBuilder.DeleteData(
                table: "Requests",
                keyColumn: "RequestId",
                keyValue: new Guid("5e160d91-4944-4212-8ae2-e127ab890908"));

            migrationBuilder.DeleteData(
                table: "Requests",
                keyColumn: "RequestId",
                keyValue: new Guid("6f9f961d-5357-49e8-82f9-d27bc7e28ab0"));

            migrationBuilder.DeleteData(
                table: "Requests",
                keyColumn: "RequestId",
                keyValue: new Guid("76447fdb-d376-4d5e-8c27-2e998b37b6c2"));

            migrationBuilder.DeleteData(
                table: "Requests",
                keyColumn: "RequestId",
                keyValue: new Guid("80d50406-a74d-4ab8-8428-857448cad775"));

            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    UserId = table.Column<Guid>(nullable: false),
                    Type = table.Column<string>(nullable: true),
                    PassWord = table.Column<string>(nullable: true),
                    Role = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.UserId);
                });

            migrationBuilder.InsertData(
                table: "Fundses",
                columns: new[] { "FundsId", "Content", "Cost", "Income", "Total" },
                values: new object[,]
                {
                    { new Guid("cfced8d5-e03f-4dd3-bffa-25b511d40fe8"), "a", 12.0, 13.0, 123.0 },
                    { new Guid("7ed5f72c-138c-4cbf-b7ec-91c623dc8b9e"), "b", 11.0, 11.0, 111.0 },
                    { new Guid("5a7c35b2-eea4-4957-96c2-706ff3d0c5c0"), "c", 9.0, 28.0, 1985.0 },
                    { new Guid("85e50cd1-8c0c-432d-8eb4-b56636e18843"), "d", 12.0, 13.0, 222.0 }
                });

            migrationBuilder.InsertData(
                table: "Requests",
                columns: new[] { "RequestId", "AdminId", "BookId", "Content", "IsApprove", "MemberId" },
                values: new object[,]
                {
                    { new Guid("c7185df8-3119-4300-893a-1354260ef8d9"), new Guid("ea434506-3a83-437f-a4b8-924c13ed9bef"), new Guid("0fafd425-0327-4f34-b086-1e519be6b81a"), "Content1", false, new Guid("cd480f72-8e38-47ec-a6b7-b2aa13208250") },
                    { new Guid("217a3762-cf53-4fb3-bdc9-26fc31f4857e"), new Guid("a9eb6fcc-bcdf-46d8-ab84-bde990836420"), new Guid("4bc1f510-2506-4894-8bba-2fe243c1e67c"), "Content2", false, new Guid("41e22dc7-be86-4030-8c62-acc81afc7d57") },
                    { new Guid("7d9d5b4d-bb19-41ea-91b3-9f1f13bdc94b"), new Guid("1a28d58d-8a89-4f01-b244-82b435a348e7"), new Guid("cca80e6b-cbb5-4938-8e61-a368a5bcee5d"), "Content3", false, new Guid("ee2cb668-d762-4faf-bbc1-34ac22005dcf") },
                    { new Guid("1cded10b-a187-4a59-83f8-4b4f05fc6245"), new Guid("814e0293-5dfe-4b52-9fd4-eaa12595e415"), new Guid("92ffa226-987f-446f-80c6-a12524fbce2a"), "Content4", false, new Guid("11d26f77-0101-4b71-94fc-baa019072765") },
                    { new Guid("0866188e-7ab2-4d70-8cc9-8ce010298427"), new Guid("29ec2e80-67e6-4c87-91de-f065a17ae27e"), new Guid("e1abe466-3a3f-4c41-a5c5-449f190dbdf8"), "Content5", false, new Guid("19600866-0bb4-4707-af70-7a0f7afe0014") },
                    { new Guid("d92ab63d-9acd-4227-b3be-d92a861b1cbf"), new Guid("96085b46-5218-48f4-933a-02307b446583"), new Guid("705db47e-8b2b-49a0-878e-79af6d74a80e"), "Content6", false, new Guid("a0c71e54-24fc-4cbf-a9c2-a2b72de70551") }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Users");

            migrationBuilder.DeleteData(
                table: "Fundses",
                keyColumn: "FundsId",
                keyValue: new Guid("5a7c35b2-eea4-4957-96c2-706ff3d0c5c0"));

            migrationBuilder.DeleteData(
                table: "Fundses",
                keyColumn: "FundsId",
                keyValue: new Guid("7ed5f72c-138c-4cbf-b7ec-91c623dc8b9e"));

            migrationBuilder.DeleteData(
                table: "Fundses",
                keyColumn: "FundsId",
                keyValue: new Guid("85e50cd1-8c0c-432d-8eb4-b56636e18843"));

            migrationBuilder.DeleteData(
                table: "Fundses",
                keyColumn: "FundsId",
                keyValue: new Guid("cfced8d5-e03f-4dd3-bffa-25b511d40fe8"));

            migrationBuilder.DeleteData(
                table: "Requests",
                keyColumn: "RequestId",
                keyValue: new Guid("0866188e-7ab2-4d70-8cc9-8ce010298427"));

            migrationBuilder.DeleteData(
                table: "Requests",
                keyColumn: "RequestId",
                keyValue: new Guid("1cded10b-a187-4a59-83f8-4b4f05fc6245"));

            migrationBuilder.DeleteData(
                table: "Requests",
                keyColumn: "RequestId",
                keyValue: new Guid("217a3762-cf53-4fb3-bdc9-26fc31f4857e"));

            migrationBuilder.DeleteData(
                table: "Requests",
                keyColumn: "RequestId",
                keyValue: new Guid("7d9d5b4d-bb19-41ea-91b3-9f1f13bdc94b"));

            migrationBuilder.DeleteData(
                table: "Requests",
                keyColumn: "RequestId",
                keyValue: new Guid("c7185df8-3119-4300-893a-1354260ef8d9"));

            migrationBuilder.DeleteData(
                table: "Requests",
                keyColumn: "RequestId",
                keyValue: new Guid("d92ab63d-9acd-4227-b3be-d92a861b1cbf"));

            migrationBuilder.InsertData(
                table: "Fundses",
                columns: new[] { "FundsId", "Content", "Cost", "Income", "Total" },
                values: new object[,]
                {
                    { new Guid("20fc6ae0-4bc4-4853-9182-0b4f7275b6c4"), "a", 12.0, 13.0, 123.0 },
                    { new Guid("52fcb781-437f-4750-a0b4-b05eda3df276"), "b", 11.0, 11.0, 111.0 },
                    { new Guid("ae8c7c98-3a10-4dde-bea4-77d8ba3f9e9b"), "c", 9.0, 28.0, 1985.0 },
                    { new Guid("9b98a78e-1bc9-482a-a77e-32782794bc07"), "d", 12.0, 13.0, 222.0 }
                });

            migrationBuilder.InsertData(
                table: "Requests",
                columns: new[] { "RequestId", "AdminId", "BookId", "Content", "IsApprove", "MemberId" },
                values: new object[,]
                {
                    { new Guid("76447fdb-d376-4d5e-8c27-2e998b37b6c2"), new Guid("f8de6205-27d6-4765-b033-4e4fddd85f48"), new Guid("5ac1598b-6f98-460f-902b-8c7e5fc5b1e8"), "Content1", false, new Guid("b3b6e977-2003-49c7-87f1-b4537c983bc7") },
                    { new Guid("5e160d91-4944-4212-8ae2-e127ab890908"), new Guid("625a4034-0ed0-4037-886c-ab099dc6e997"), new Guid("5e677880-6038-4c01-bd4d-0a06ae379226"), "Content2", false, new Guid("2f4c8dfd-01e3-4876-9cd4-e22b341e7f54") },
                    { new Guid("80d50406-a74d-4ab8-8428-857448cad775"), new Guid("df1e9db6-8cdd-4bd5-a327-89b2a6e21f73"), new Guid("bf1da96e-2f64-493c-b572-7d7fa9873aae"), "Content3", false, new Guid("939928b1-7a90-4688-807c-9262486cf0cd") },
                    { new Guid("17ee3be0-8568-4636-97f5-a99d3ef59270"), new Guid("3212ccd7-1e91-4fd7-a41b-eb0e54c4164b"), new Guid("e432e130-fefc-42ba-baf6-0852eaa1972c"), "Content4", false, new Guid("44a1ddbe-311a-4978-a4aa-4fbfc8200be6") },
                    { new Guid("21e2ee69-c9bb-4b92-b216-237cd07c1df3"), new Guid("aa2c5154-d081-4a8c-b30e-c6f93a8ece76"), new Guid("51705130-a18b-42f1-b64f-e19cf5407abe"), "Content5", false, new Guid("da09d16a-266e-4f02-8890-4fcdc8bed32c") },
                    { new Guid("6f9f961d-5357-49e8-82f9-d27bc7e28ab0"), new Guid("567573fe-08a1-464a-98c0-99f2da9ad5e7"), new Guid("345eb844-a509-4c9c-9edd-1a8146e9837e"), "Content6", false, new Guid("845335df-9bac-4ed6-9490-6edfab540736") }
                });
        }
    }
}
