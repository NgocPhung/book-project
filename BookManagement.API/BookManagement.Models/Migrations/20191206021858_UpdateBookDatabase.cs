﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace BookManagement.Models.Migrations
{
    public partial class UpdateBookDatabase : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "RequestName",
                table: "Requests",
                newName: "Content");

            migrationBuilder.RenameColumn(
                name: "Book",
                table: "Requests",
                newName: "BookId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Content",
                table: "Requests",
                newName: "RequestName");

            migrationBuilder.RenameColumn(
                name: "BookId",
                table: "Requests",
                newName: "Book");
        }
    }
}
