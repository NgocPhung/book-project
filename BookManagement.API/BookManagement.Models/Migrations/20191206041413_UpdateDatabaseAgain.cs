﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace BookManagement.Models.Migrations
{
    public partial class UpdateDatabaseAgain : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Fundses",
                columns: table => new
                {
                    FundsId = table.Column<Guid>(nullable: false),
                    Total = table.Column<double>(nullable: false),
                    Cost = table.Column<double>(nullable: false),
                    Income = table.Column<double>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Fundses", x => x.FundsId);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Fundses");

            
        }
    }
}
