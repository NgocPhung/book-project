﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace BookManagement.Models.Migrations
{
    public partial class InsertFundsTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Requests",
                keyColumn: "RequestId",
                keyValue: new Guid("3acfabef-36ed-4db7-8347-34f271439406"));

            migrationBuilder.DeleteData(
                table: "Requests",
                keyColumn: "RequestId",
                keyValue: new Guid("4f3fa940-5977-4bf8-9759-d9e263541b2d"));

            migrationBuilder.DeleteData(
                table: "Requests",
                keyColumn: "RequestId",
                keyValue: new Guid("514ca419-c5c2-4c8b-bf61-71ec1340486d"));

            migrationBuilder.DeleteData(
                table: "Requests",
                keyColumn: "RequestId",
                keyValue: new Guid("7089f720-b1bd-4cf9-8599-03e65ed9eedc"));

            migrationBuilder.DeleteData(
                table: "Requests",
                keyColumn: "RequestId",
                keyValue: new Guid("7436fec2-3da5-4e66-afa8-d9727ea18a82"));

            migrationBuilder.DeleteData(
                table: "Requests",
                keyColumn: "RequestId",
                keyValue: new Guid("d9710225-8f6f-41c0-a1d0-562fb7e7d894"));

            migrationBuilder.InsertData(
                table: "Fundses",
                columns: new[] { "FundsId", "Cost", "Income", "Total" },
                values: new object[,]
                {
                    { new Guid("68c43e91-ea3f-4b91-9703-546b818d95ed"), 12.0, 13.0, 123.0 },
                    { new Guid("4c205297-b2d7-4f82-9fc4-06f1eb574326"), 11.0, 11.0, 111.0 },
                    { new Guid("6ca73a92-6824-4160-8a4e-ccb3af2e187c"), 9.0, 28.0, 1985.0 },
                    { new Guid("19538515-1b8d-4b98-9212-8a909e906f15"), 12.0, 13.0, 222.0 }
                });

            migrationBuilder.InsertData(
                table: "Requests",
                columns: new[] { "RequestId", "AdminId", "BookId", "Content", "MemberId" },
                values: new object[,]
                {
                    { new Guid("fbeaf197-7389-4028-a1c6-7b997a88f331"), new Guid("9a09ac81-bd1b-4a0a-8e09-9e42d306c55a"), new Guid("ca51dcf8-475b-44e2-a4b6-30494015a70c"), "Content1", new Guid("6ce6719a-feab-4505-b8f1-7c9c46f8f883") },
                    { new Guid("b540bdeb-c548-4a78-b22d-972251bce6ef"), new Guid("d3983e8e-d587-4429-bf4e-499f2ee21a7e"), new Guid("22fc4884-b7e5-40ee-9869-73c9993658a9"), "Content2", new Guid("86110ebe-0964-4fdd-982c-d410890fae15") },
                    { new Guid("686d4294-152a-4c60-806c-ebc870504454"), new Guid("e7e93570-58d9-4acd-887d-5aaf634ee0ab"), new Guid("5c41c355-6b1f-461e-acd9-979ba19811c6"), "Content3", new Guid("28fed54e-03dc-4ee0-a2a6-e236627cdd46") },
                    { new Guid("22d3f430-eeac-44a8-bd0c-517ade2fb0db"), new Guid("d3aadf63-56d0-495e-b6aa-0ea15d1ce539"), new Guid("017d7b94-298e-4f72-ba71-849c8c52b5cf"), "Content4", new Guid("49995da1-0f4e-4106-af88-27922a4af132") },
                    { new Guid("e926bf69-c818-4138-8cb7-d9ca25852b19"), new Guid("759ae02d-26fb-45c7-b24b-04b458759981"), new Guid("ea9e875c-0fbe-44b4-83d6-de1c24898ca9"), "Content5", new Guid("b3d8dea5-f75a-4be5-ba68-6708150a7a0b") },
                    { new Guid("6a1a6f50-aefb-4d51-8735-77e226e44a86"), new Guid("ddb04b03-cc52-456c-9a31-8b3021f1a468"), new Guid("622658a4-eebd-4450-8391-12fc1d7636c7"), "Content6", new Guid("a3651eaf-8d47-4053-9f62-58558d415f70") }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Fundses",
                keyColumn: "FundsId",
                keyValue: new Guid("19538515-1b8d-4b98-9212-8a909e906f15"));

            migrationBuilder.DeleteData(
                table: "Fundses",
                keyColumn: "FundsId",
                keyValue: new Guid("4c205297-b2d7-4f82-9fc4-06f1eb574326"));

            migrationBuilder.DeleteData(
                table: "Fundses",
                keyColumn: "FundsId",
                keyValue: new Guid("68c43e91-ea3f-4b91-9703-546b818d95ed"));

            migrationBuilder.DeleteData(
                table: "Fundses",
                keyColumn: "FundsId",
                keyValue: new Guid("6ca73a92-6824-4160-8a4e-ccb3af2e187c"));

            migrationBuilder.DeleteData(
                table: "Requests",
                keyColumn: "RequestId",
                keyValue: new Guid("22d3f430-eeac-44a8-bd0c-517ade2fb0db"));

            migrationBuilder.DeleteData(
                table: "Requests",
                keyColumn: "RequestId",
                keyValue: new Guid("686d4294-152a-4c60-806c-ebc870504454"));

            migrationBuilder.DeleteData(
                table: "Requests",
                keyColumn: "RequestId",
                keyValue: new Guid("6a1a6f50-aefb-4d51-8735-77e226e44a86"));

            migrationBuilder.DeleteData(
                table: "Requests",
                keyColumn: "RequestId",
                keyValue: new Guid("b540bdeb-c548-4a78-b22d-972251bce6ef"));

            migrationBuilder.DeleteData(
                table: "Requests",
                keyColumn: "RequestId",
                keyValue: new Guid("e926bf69-c818-4138-8cb7-d9ca25852b19"));

            migrationBuilder.DeleteData(
                table: "Requests",
                keyColumn: "RequestId",
                keyValue: new Guid("fbeaf197-7389-4028-a1c6-7b997a88f331"));

            migrationBuilder.InsertData(
                table: "Requests",
                columns: new[] { "RequestId", "AdminId", "BookId", "Content", "MemberId" },
                values: new object[,]
                {
                    { new Guid("d9710225-8f6f-41c0-a1d0-562fb7e7d894"), new Guid("d796edc7-69c3-490b-8aae-6d3d71fa0bdb"), new Guid("36acb664-6db6-4b66-b31b-7ca31c659fe2"), "Content1", new Guid("f28783c6-14ef-456e-b812-dc1922a454b9") },
                    { new Guid("514ca419-c5c2-4c8b-bf61-71ec1340486d"), new Guid("d8b0f6a7-d9dd-4be1-8eaf-2308312ce0ce"), new Guid("d38b7e8d-37b1-49a2-a2bd-0c60c4379963"), "Content2", new Guid("fd177cac-0f09-44cd-b2c0-f017dfd25c12") },
                    { new Guid("3acfabef-36ed-4db7-8347-34f271439406"), new Guid("375c611b-2ccd-46ad-aa32-6106bd23ef5b"), new Guid("6e0aba85-7bd1-4514-af73-23ae91c31f60"), "Content3", new Guid("9d0f9d22-1855-495d-a9fb-b05fd9e06bef") },
                    { new Guid("7089f720-b1bd-4cf9-8599-03e65ed9eedc"), new Guid("ada49edc-67de-40f7-a3bd-e1015393a3a6"), new Guid("c641fbfe-c826-499d-81e8-c261920c02b1"), "Content4", new Guid("10f0406f-f2b0-424a-a7d1-1a4f0731c187") },
                    { new Guid("7436fec2-3da5-4e66-afa8-d9727ea18a82"), new Guid("889e9aa9-f66f-4678-b971-8e930a9c3ab5"), new Guid("4c1a2270-f2ad-49cb-b36c-a65e13683300"), "Content5", new Guid("92785e96-c9ee-4ac4-a930-d9ff8b40acf7") },
                    { new Guid("4f3fa940-5977-4bf8-9759-d9e263541b2d"), new Guid("ffdeaa6d-c190-4f2c-9eea-b4a878feb4a1"), new Guid("7786f8fb-775c-4c29-952b-439937929ddb"), "Content6", new Guid("9961ac55-46c9-4abb-bcff-9953e7c75115") }
                });
        }
    }
}
