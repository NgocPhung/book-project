﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace BookManagement.Models.Migrations
{
    public partial class UpdateAndAlterDatabaseAgain : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Fundses",
                keyColumn: "FundsId",
                keyValue: new Guid("422e978b-b3a2-496a-a082-f1b54bcfb98f"));

            migrationBuilder.DeleteData(
                table: "Fundses",
                keyColumn: "FundsId",
                keyValue: new Guid("ac6a03d9-380f-45a3-b2cb-937594344ee1"));

            migrationBuilder.DeleteData(
                table: "Fundses",
                keyColumn: "FundsId",
                keyValue: new Guid("d1c167da-4336-4457-9a1e-b29563c31ca2"));

            migrationBuilder.DeleteData(
                table: "Fundses",
                keyColumn: "FundsId",
                keyValue: new Guid("d565a9ab-3999-4585-8c18-c072f084f1d5"));

            migrationBuilder.DeleteData(
                table: "Requests",
                keyColumn: "RequestId",
                keyValue: new Guid("4072ac44-a721-4841-a865-89380cd3f093"));

            migrationBuilder.DeleteData(
                table: "Requests",
                keyColumn: "RequestId",
                keyValue: new Guid("9c7a1c27-88e3-45ad-a84b-b0c622789368"));

            migrationBuilder.DeleteData(
                table: "Requests",
                keyColumn: "RequestId",
                keyValue: new Guid("af0696e2-1bd1-47f9-aa53-6b007b6c0bbd"));

            migrationBuilder.DeleteData(
                table: "Requests",
                keyColumn: "RequestId",
                keyValue: new Guid("d3838197-3163-4c21-b0cf-a4d37b41ea56"));

            migrationBuilder.DeleteData(
                table: "Requests",
                keyColumn: "RequestId",
                keyValue: new Guid("e5b8b1c2-541c-4b52-ba88-738c2b5ec703"));

            migrationBuilder.DeleteData(
                table: "Requests",
                keyColumn: "RequestId",
                keyValue: new Guid("f5e62be3-aec5-48e8-b258-0b15c98705bb"));

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "UserId",
                keyValue: new Guid("5adc2529-4a84-4883-ba54-bf9c08f4a3bf"));

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "UserId",
                keyValue: new Guid("74ceb1d8-f32b-467b-9f2b-d399e36a1321"));

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "UserId",
                keyValue: new Guid("c0199718-af89-4681-bccf-cbe24992098d"));

            migrationBuilder.AddColumn<Guid>(
                name: "UserId",
                table: "Incomes",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<Guid>(
                name: "BookId",
                table: "Costs",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.InsertData(
                table: "Costs",
                columns: new[] { "CostId", "BookId", "Content", "CostMoney", "CreatedDate" },
                values: new object[,]
                {
                    { new Guid("8bf29293-ffcf-41c0-9fe5-f66fd8d1129c"), new Guid("587f6f7a-bac4-41e4-a19b-822868d66fff"), "Mua sach A", 20000.0, new DateTime(2019, 12, 23, 9, 15, 33, 217, DateTimeKind.Local).AddTicks(4815) },
                    { new Guid("b9cb4228-c3e8-4d46-9e7b-d39d5f64af47"), new Guid("eca675ce-1a38-4325-b63e-d1cfcffd4c44"), "Mua sach B", 10000.0, new DateTime(2019, 12, 23, 9, 15, 33, 218, DateTimeKind.Local).AddTicks(5975) },
                    { new Guid("d0cde227-0a6a-4b9b-9ba6-4b7122c15914"), new Guid("4555899c-1188-4956-b0e1-9f491e470988"), "Mua sach C", 50000.0, new DateTime(2019, 12, 23, 9, 15, 33, 218, DateTimeKind.Local).AddTicks(6136) },
                    { new Guid("3ba4d324-356a-44c0-83fe-93a3991d7ee5"), new Guid("f5a6bb3b-df9d-4a7e-a81b-cdd5479decd9"), "Mua sach D", 60000.0, new DateTime(2019, 12, 23, 9, 15, 33, 218, DateTimeKind.Local).AddTicks(6189) }
                });

            migrationBuilder.InsertData(
                table: "Fundses",
                columns: new[] { "FundsId", "Cost", "Income", "Total" },
                values: new object[,]
                {
                    { new Guid("72d3ae1d-44c8-489d-81bb-fd0728cb4e8d"), 12.0, 13.0, 1.0 },
                    { new Guid("621dc2b4-399b-46e6-ac93-7c8c44042a5b"), 111.0, 11.0, -100.0 },
                    { new Guid("9fac1f04-4ddf-4a08-8b2b-8c9236309b06"), 9.0, 28.0, 19.0 },
                    { new Guid("55affe75-9e83-407b-b95f-c4e6d14bf6e0"), 222.0, 12.0, -210.0 }
                });

            migrationBuilder.InsertData(
                table: "Incomes",
                columns: new[] { "IncomeId", "Content", "CreatedDate", "IncometMoney", "UserId" },
                values: new object[,]
                {
                    { new Guid("35782663-32c7-4767-9f74-3fdf4618ba15"), "D nop quy thang 12", new DateTime(2019, 12, 23, 9, 15, 33, 219, DateTimeKind.Local).AddTicks(1943), 160000.0, new Guid("427a57fc-a52b-4a3f-b364-e05164a96f81") },
                    { new Guid("1e227a93-ddbe-400b-ac17-d51689ce2a0c"), "C nop quy thang 12", new DateTime(2019, 12, 23, 9, 15, 33, 219, DateTimeKind.Local).AddTicks(1897), 150000.0, new Guid("6a1d726b-f12f-4659-be12-cfb206fe0233") },
                    { new Guid("a10e832b-8542-4518-8a8b-55358656143d"), "A nop quy thang 12", new DateTime(2019, 12, 23, 9, 15, 33, 218, DateTimeKind.Local).AddTicks(7159), 200000.0, new Guid("5413a1d8-6603-44c1-8890-705f672969fa") },
                    { new Guid("75a9d569-13d3-429d-9e53-0636aa64b08c"), "B nop quy thang 12", new DateTime(2019, 12, 23, 9, 15, 33, 219, DateTimeKind.Local).AddTicks(1787), 100000.0, new Guid("bacdd5d5-00b5-4f71-aea5-ee2c9c63292e") }
                });

            migrationBuilder.InsertData(
                table: "Requests",
                columns: new[] { "RequestId", "AdminId", "BookId", "Content", "IsApprove", "MemberId" },
                values: new object[,]
                {
                    { new Guid("1bc558a7-dcf2-4ea0-a87b-72d0aa91c192"), new Guid("753b44d3-fa69-43bb-8b55-b9331c418431"), new Guid("51fa2b41-e65b-406b-9d4a-95b298f84042"), "Content1", false, new Guid("16865108-f9f7-4269-83d0-fdf89528df62") },
                    { new Guid("eb48a567-ea2c-4737-ba81-c543a16459f8"), new Guid("dec9425f-0361-4d6c-8f70-b43cb3b59d9f"), new Guid("a73f88df-8a46-40dc-96e8-c62f2d6460d5"), "Content2", false, new Guid("51c309da-9549-4b64-a9aa-921c209c15d8") },
                    { new Guid("facd8aeb-f1ec-4f07-a7b1-1f5ed9ced891"), new Guid("97cdf087-26e2-45d4-b39f-ec8988e30322"), new Guid("73f2c8ce-fb76-4184-9fb8-0df6ce585913"), "Content3", false, new Guid("5b8d4159-e7e3-4e89-b9ef-8e0294c5579d") },
                    { new Guid("765c4dd7-9635-4180-8a91-8e161618c7f0"), new Guid("4ab6501e-5687-4c59-8767-e9140cac948c"), new Guid("82eec1ff-a24a-4171-9180-94e7aafd1107"), "Content4", false, new Guid("cc152c40-4cd0-41ca-9ec3-8e1d81a5ed5e") },
                    { new Guid("00e140f1-648e-4862-91f5-6cdb155a631b"), new Guid("ce613195-410f-4364-b3e5-5ad11353cd22"), new Guid("008b8937-50c8-4ddd-8c9c-975aac56be18"), "Content5", false, new Guid("e53fbbb9-455b-4f80-bd83-05ac4938ddb5") },
                    { new Guid("9ced8592-ed11-4993-8eea-a46b48ee696e"), new Guid("c8c0e09d-54ce-46de-85a4-795d019cdc5b"), new Guid("5711622e-df06-46d6-a8cb-60d96fbea545"), "Content6", false, new Guid("7d006f67-2d77-461d-8574-bc26dc57004f") }
                });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "UserId", "Email", "PassWord", "Role" },
                values: new object[,]
                {
                    { new Guid("3be70066-b35e-4c36-8ebb-a18d0ee241ac"), "dtnp@gmail.com", "123456", "Member" },
                    { new Guid("e6e8521d-3211-4f42-9541-f76d88a8d03d"), "abc@gmail.com", "123456", "Admin" },
                    { new Guid("b956124e-1bff-4ee3-b03a-8111239f80a3"), "mm@gmail.com", "123456", "Member" }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Costs",
                keyColumn: "CostId",
                keyValue: new Guid("3ba4d324-356a-44c0-83fe-93a3991d7ee5"));

            migrationBuilder.DeleteData(
                table: "Costs",
                keyColumn: "CostId",
                keyValue: new Guid("8bf29293-ffcf-41c0-9fe5-f66fd8d1129c"));

            migrationBuilder.DeleteData(
                table: "Costs",
                keyColumn: "CostId",
                keyValue: new Guid("b9cb4228-c3e8-4d46-9e7b-d39d5f64af47"));

            migrationBuilder.DeleteData(
                table: "Costs",
                keyColumn: "CostId",
                keyValue: new Guid("d0cde227-0a6a-4b9b-9ba6-4b7122c15914"));

            migrationBuilder.DeleteData(
                table: "Fundses",
                keyColumn: "FundsId",
                keyValue: new Guid("55affe75-9e83-407b-b95f-c4e6d14bf6e0"));

            migrationBuilder.DeleteData(
                table: "Fundses",
                keyColumn: "FundsId",
                keyValue: new Guid("621dc2b4-399b-46e6-ac93-7c8c44042a5b"));

            migrationBuilder.DeleteData(
                table: "Fundses",
                keyColumn: "FundsId",
                keyValue: new Guid("72d3ae1d-44c8-489d-81bb-fd0728cb4e8d"));

            migrationBuilder.DeleteData(
                table: "Fundses",
                keyColumn: "FundsId",
                keyValue: new Guid("9fac1f04-4ddf-4a08-8b2b-8c9236309b06"));

            migrationBuilder.DeleteData(
                table: "Incomes",
                keyColumn: "IncomeId",
                keyValue: new Guid("1e227a93-ddbe-400b-ac17-d51689ce2a0c"));

            migrationBuilder.DeleteData(
                table: "Incomes",
                keyColumn: "IncomeId",
                keyValue: new Guid("35782663-32c7-4767-9f74-3fdf4618ba15"));

            migrationBuilder.DeleteData(
                table: "Incomes",
                keyColumn: "IncomeId",
                keyValue: new Guid("75a9d569-13d3-429d-9e53-0636aa64b08c"));

            migrationBuilder.DeleteData(
                table: "Incomes",
                keyColumn: "IncomeId",
                keyValue: new Guid("a10e832b-8542-4518-8a8b-55358656143d"));

            migrationBuilder.DeleteData(
                table: "Requests",
                keyColumn: "RequestId",
                keyValue: new Guid("00e140f1-648e-4862-91f5-6cdb155a631b"));

            migrationBuilder.DeleteData(
                table: "Requests",
                keyColumn: "RequestId",
                keyValue: new Guid("1bc558a7-dcf2-4ea0-a87b-72d0aa91c192"));

            migrationBuilder.DeleteData(
                table: "Requests",
                keyColumn: "RequestId",
                keyValue: new Guid("765c4dd7-9635-4180-8a91-8e161618c7f0"));

            migrationBuilder.DeleteData(
                table: "Requests",
                keyColumn: "RequestId",
                keyValue: new Guid("9ced8592-ed11-4993-8eea-a46b48ee696e"));

            migrationBuilder.DeleteData(
                table: "Requests",
                keyColumn: "RequestId",
                keyValue: new Guid("eb48a567-ea2c-4737-ba81-c543a16459f8"));

            migrationBuilder.DeleteData(
                table: "Requests",
                keyColumn: "RequestId",
                keyValue: new Guid("facd8aeb-f1ec-4f07-a7b1-1f5ed9ced891"));

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "UserId",
                keyValue: new Guid("3be70066-b35e-4c36-8ebb-a18d0ee241ac"));

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "UserId",
                keyValue: new Guid("b956124e-1bff-4ee3-b03a-8111239f80a3"));

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "UserId",
                keyValue: new Guid("e6e8521d-3211-4f42-9541-f76d88a8d03d"));

            migrationBuilder.DropColumn(
                name: "UserId",
                table: "Incomes");

            migrationBuilder.DropColumn(
                name: "BookId",
                table: "Costs");

            migrationBuilder.InsertData(
                table: "Fundses",
                columns: new[] { "FundsId", "Cost", "Income", "Total" },
                values: new object[,]
                {
                    { new Guid("d1c167da-4336-4457-9a1e-b29563c31ca2"), 12.0, 13.0, 1.0 },
                    { new Guid("422e978b-b3a2-496a-a082-f1b54bcfb98f"), 111.0, 11.0, -100.0 },
                    { new Guid("ac6a03d9-380f-45a3-b2cb-937594344ee1"), 9.0, 28.0, 19.0 },
                    { new Guid("d565a9ab-3999-4585-8c18-c072f084f1d5"), 222.0, 12.0, -210.0 }
                });

            migrationBuilder.InsertData(
                table: "Requests",
                columns: new[] { "RequestId", "AdminId", "BookId", "Content", "IsApprove", "MemberId" },
                values: new object[,]
                {
                    { new Guid("af0696e2-1bd1-47f9-aa53-6b007b6c0bbd"), new Guid("220574d3-22a6-4fb2-9be6-0edc178c7764"), new Guid("03739ecb-92eb-4556-b209-602051a7c17b"), "Content1", false, new Guid("d17c2516-9b1a-4f1f-b92d-a5a8fd4e0998") },
                    { new Guid("d3838197-3163-4c21-b0cf-a4d37b41ea56"), new Guid("944d7c32-cc55-4ee2-b88b-ac2f60e13cb4"), new Guid("51a2c551-1faa-4f22-9e02-a178aa2620f3"), "Content2", false, new Guid("f406506a-8383-4131-b179-648053127049") },
                    { new Guid("f5e62be3-aec5-48e8-b258-0b15c98705bb"), new Guid("9a33e212-b542-45f0-a613-9b4ecc944ae9"), new Guid("1e59f72e-8176-43cc-b23d-e7eb36445cb1"), "Content3", false, new Guid("83e051af-674d-412b-98a8-78e7c5efecdc") },
                    { new Guid("9c7a1c27-88e3-45ad-a84b-b0c622789368"), new Guid("8c13ff09-0dcb-4251-9e6f-ec0beab2c917"), new Guid("283b0c36-2466-4fb3-8c2f-6a5b9366b741"), "Content4", false, new Guid("d323eca1-dbf7-4846-b641-b2e6dc9be7e9") },
                    { new Guid("e5b8b1c2-541c-4b52-ba88-738c2b5ec703"), new Guid("a437d7b5-06f6-4e93-b9e4-3842279164f6"), new Guid("a0a88903-312e-4604-9327-57f83cc2eff6"), "Content5", false, new Guid("979ac784-71a8-4412-8490-d5ca34ea48a6") },
                    { new Guid("4072ac44-a721-4841-a865-89380cd3f093"), new Guid("692087ae-74d0-48e1-a5c9-ebc4abe1454c"), new Guid("4ef0520c-950d-4096-8025-734cd8b98c3a"), "Content6", false, new Guid("79fe78a1-9358-474b-80b5-f50a55fd0828") }
                });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "UserId", "Email", "PassWord", "Role" },
                values: new object[,]
                {
                    { new Guid("74ceb1d8-f32b-467b-9f2b-d399e36a1321"), "abc@gmail.com", "123456", "Admin" },
                    { new Guid("c0199718-af89-4681-bccf-cbe24992098d"), "dtnp@gmail.com", "123456", "Member" },
                    { new Guid("5adc2529-4a84-4883-ba54-bf9c08f4a3bf"), "mm@gmail.com", "123456", "Member" }
                });
        }
    }
}
