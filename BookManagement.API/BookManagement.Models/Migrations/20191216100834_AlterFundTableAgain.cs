﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace BookManagement.Models.Migrations
{
    public partial class AlterFundTableAgain : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Fundses",
                keyColumn: "FundsId",
                keyValue: new Guid("4333b4bf-38e9-4be5-a694-5e2cfc3f39d4"));

            migrationBuilder.DeleteData(
                table: "Fundses",
                keyColumn: "FundsId",
                keyValue: new Guid("61e59302-2735-4471-bf2d-dba47414231a"));

            migrationBuilder.DeleteData(
                table: "Fundses",
                keyColumn: "FundsId",
                keyValue: new Guid("e5df01ee-a6a0-4150-96b4-9b0357b7abc8"));

            migrationBuilder.DeleteData(
                table: "Fundses",
                keyColumn: "FundsId",
                keyValue: new Guid("ec34699b-fc85-4bc5-95fa-4491ee72b7af"));

            migrationBuilder.DeleteData(
                table: "Requests",
                keyColumn: "RequestId",
                keyValue: new Guid("13c5aec6-8e2b-424d-8aeb-1337bf722da6"));

            migrationBuilder.DeleteData(
                table: "Requests",
                keyColumn: "RequestId",
                keyValue: new Guid("18eea785-a34b-465b-a4fa-901044d9879b"));

            migrationBuilder.DeleteData(
                table: "Requests",
                keyColumn: "RequestId",
                keyValue: new Guid("1903aab2-cf91-4955-90ee-3210867d973d"));

            migrationBuilder.DeleteData(
                table: "Requests",
                keyColumn: "RequestId",
                keyValue: new Guid("431442eb-9ef3-4ee8-ae32-32d0462c8de0"));

            migrationBuilder.DeleteData(
                table: "Requests",
                keyColumn: "RequestId",
                keyValue: new Guid("9bff9d13-d8e6-403d-b996-ec5922de53d5"));

            migrationBuilder.DeleteData(
                table: "Requests",
                keyColumn: "RequestId",
                keyValue: new Guid("c6f47758-a62d-40a2-ab7d-94a28d4aa972"));

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "UserId",
                keyValue: new Guid("15874b96-f56c-4eda-ab58-0e8b3c07716f"));

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "UserId",
                keyValue: new Guid("7014e07d-aee1-4691-a977-9bbb54fd5a3c"));

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "UserId",
                keyValue: new Guid("b3fc0a57-556c-4046-abd9-a98e8ff02b2a"));

            migrationBuilder.DropColumn(
                name: "Content",
                table: "Fundses");

            migrationBuilder.InsertData(
                table: "Fundses",
                columns: new[] { "FundsId", "Cost", "Income", "Total" },
                values: new object[,]
                {
                    { new Guid("d1c167da-4336-4457-9a1e-b29563c31ca2"), 12.0, 13.0, 1.0 },
                    { new Guid("422e978b-b3a2-496a-a082-f1b54bcfb98f"), 111.0, 11.0, -100.0 },
                    { new Guid("ac6a03d9-380f-45a3-b2cb-937594344ee1"), 9.0, 28.0, 19.0 },
                    { new Guid("d565a9ab-3999-4585-8c18-c072f084f1d5"), 222.0, 12.0, -210.0 }
                });

            migrationBuilder.InsertData(
                table: "Requests",
                columns: new[] { "RequestId", "AdminId", "BookId", "Content", "IsApprove", "MemberId" },
                values: new object[,]
                {
                    { new Guid("af0696e2-1bd1-47f9-aa53-6b007b6c0bbd"), new Guid("220574d3-22a6-4fb2-9be6-0edc178c7764"), new Guid("03739ecb-92eb-4556-b209-602051a7c17b"), "Content1", false, new Guid("d17c2516-9b1a-4f1f-b92d-a5a8fd4e0998") },
                    { new Guid("d3838197-3163-4c21-b0cf-a4d37b41ea56"), new Guid("944d7c32-cc55-4ee2-b88b-ac2f60e13cb4"), new Guid("51a2c551-1faa-4f22-9e02-a178aa2620f3"), "Content2", false, new Guid("f406506a-8383-4131-b179-648053127049") },
                    { new Guid("f5e62be3-aec5-48e8-b258-0b15c98705bb"), new Guid("9a33e212-b542-45f0-a613-9b4ecc944ae9"), new Guid("1e59f72e-8176-43cc-b23d-e7eb36445cb1"), "Content3", false, new Guid("83e051af-674d-412b-98a8-78e7c5efecdc") },
                    { new Guid("9c7a1c27-88e3-45ad-a84b-b0c622789368"), new Guid("8c13ff09-0dcb-4251-9e6f-ec0beab2c917"), new Guid("283b0c36-2466-4fb3-8c2f-6a5b9366b741"), "Content4", false, new Guid("d323eca1-dbf7-4846-b641-b2e6dc9be7e9") },
                    { new Guid("e5b8b1c2-541c-4b52-ba88-738c2b5ec703"), new Guid("a437d7b5-06f6-4e93-b9e4-3842279164f6"), new Guid("a0a88903-312e-4604-9327-57f83cc2eff6"), "Content5", false, new Guid("979ac784-71a8-4412-8490-d5ca34ea48a6") },
                    { new Guid("4072ac44-a721-4841-a865-89380cd3f093"), new Guid("692087ae-74d0-48e1-a5c9-ebc4abe1454c"), new Guid("4ef0520c-950d-4096-8025-734cd8b98c3a"), "Content6", false, new Guid("79fe78a1-9358-474b-80b5-f50a55fd0828") }
                });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "UserId", "Email", "PassWord", "Role" },
                values: new object[,]
                {
                    { new Guid("74ceb1d8-f32b-467b-9f2b-d399e36a1321"), "abc@gmail.com", "123456", "Admin" },
                    { new Guid("c0199718-af89-4681-bccf-cbe24992098d"), "dtnp@gmail.com", "123456", "Member" },
                    { new Guid("5adc2529-4a84-4883-ba54-bf9c08f4a3bf"), "mm@gmail.com", "123456", "Member" }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Fundses",
                keyColumn: "FundsId",
                keyValue: new Guid("422e978b-b3a2-496a-a082-f1b54bcfb98f"));

            migrationBuilder.DeleteData(
                table: "Fundses",
                keyColumn: "FundsId",
                keyValue: new Guid("ac6a03d9-380f-45a3-b2cb-937594344ee1"));

            migrationBuilder.DeleteData(
                table: "Fundses",
                keyColumn: "FundsId",
                keyValue: new Guid("d1c167da-4336-4457-9a1e-b29563c31ca2"));

            migrationBuilder.DeleteData(
                table: "Fundses",
                keyColumn: "FundsId",
                keyValue: new Guid("d565a9ab-3999-4585-8c18-c072f084f1d5"));

            migrationBuilder.DeleteData(
                table: "Requests",
                keyColumn: "RequestId",
                keyValue: new Guid("4072ac44-a721-4841-a865-89380cd3f093"));

            migrationBuilder.DeleteData(
                table: "Requests",
                keyColumn: "RequestId",
                keyValue: new Guid("9c7a1c27-88e3-45ad-a84b-b0c622789368"));

            migrationBuilder.DeleteData(
                table: "Requests",
                keyColumn: "RequestId",
                keyValue: new Guid("af0696e2-1bd1-47f9-aa53-6b007b6c0bbd"));

            migrationBuilder.DeleteData(
                table: "Requests",
                keyColumn: "RequestId",
                keyValue: new Guid("d3838197-3163-4c21-b0cf-a4d37b41ea56"));

            migrationBuilder.DeleteData(
                table: "Requests",
                keyColumn: "RequestId",
                keyValue: new Guid("e5b8b1c2-541c-4b52-ba88-738c2b5ec703"));

            migrationBuilder.DeleteData(
                table: "Requests",
                keyColumn: "RequestId",
                keyValue: new Guid("f5e62be3-aec5-48e8-b258-0b15c98705bb"));

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "UserId",
                keyValue: new Guid("5adc2529-4a84-4883-ba54-bf9c08f4a3bf"));

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "UserId",
                keyValue: new Guid("74ceb1d8-f32b-467b-9f2b-d399e36a1321"));

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "UserId",
                keyValue: new Guid("c0199718-af89-4681-bccf-cbe24992098d"));

            migrationBuilder.AddColumn<string>(
                name: "Content",
                table: "Fundses",
                nullable: true);

            migrationBuilder.InsertData(
                table: "Fundses",
                columns: new[] { "FundsId", "Content", "Cost", "Income", "Total" },
                values: new object[,]
                {
                    { new Guid("e5df01ee-a6a0-4150-96b4-9b0357b7abc8"), "a", 12.0, 13.0, 123.0 },
                    { new Guid("4333b4bf-38e9-4be5-a694-5e2cfc3f39d4"), "b", 11.0, 11.0, 111.0 },
                    { new Guid("61e59302-2735-4471-bf2d-dba47414231a"), "c", 9.0, 28.0, 1985.0 },
                    { new Guid("ec34699b-fc85-4bc5-95fa-4491ee72b7af"), "d", 12.0, 13.0, 222.0 }
                });

            migrationBuilder.InsertData(
                table: "Requests",
                columns: new[] { "RequestId", "AdminId", "BookId", "Content", "IsApprove", "MemberId" },
                values: new object[,]
                {
                    { new Guid("18eea785-a34b-465b-a4fa-901044d9879b"), new Guid("3052fa9b-fca8-4181-9a31-883912c16448"), new Guid("e69923a8-c218-4621-bc96-52ca8fa79682"), "Content1", false, new Guid("d7cd9d08-20c9-4dcb-acc2-bf1120872b29") },
                    { new Guid("9bff9d13-d8e6-403d-b996-ec5922de53d5"), new Guid("bfbdc176-a724-42a2-a575-03f84e8481d2"), new Guid("bb7ec5c6-0cb4-4f7e-b0fa-ea4ec188c72d"), "Content2", false, new Guid("e5698645-bd52-4cb1-9e6f-f03ff997b177") },
                    { new Guid("1903aab2-cf91-4955-90ee-3210867d973d"), new Guid("7e89bfd4-530a-493f-9e02-bd7061d35939"), new Guid("fc67e1b7-b7b8-4d0b-b8dc-667924c6077c"), "Content3", false, new Guid("89323bf2-1e0e-482f-8fa6-8970180dc78d") },
                    { new Guid("13c5aec6-8e2b-424d-8aeb-1337bf722da6"), new Guid("e0f122ea-9f5e-4f1c-9ea2-a2447770d212"), new Guid("8ca147a0-a630-4e7a-91ac-dc71cdff9a50"), "Content4", false, new Guid("47b8d055-898d-4cad-ae99-2ff4dc325299") },
                    { new Guid("c6f47758-a62d-40a2-ab7d-94a28d4aa972"), new Guid("37f727f1-cbdb-448e-8dd0-01340a01276a"), new Guid("f81906b0-3795-4427-9213-0e84c16c1c8f"), "Content5", false, new Guid("0701e539-a23e-4a7f-9c0e-3eca0003881c") },
                    { new Guid("431442eb-9ef3-4ee8-ae32-32d0462c8de0"), new Guid("0aaccf01-8c2d-4e92-b102-bbed205e5788"), new Guid("b5bb5248-e327-4fe7-90af-f460c1101464"), "Content6", false, new Guid("4a90a888-676e-4671-8333-0712ad5e0882") }
                });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "UserId", "Email", "PassWord", "Role" },
                values: new object[,]
                {
                    { new Guid("7014e07d-aee1-4691-a977-9bbb54fd5a3c"), "abc@gmail.com", "123456", "Admin" },
                    { new Guid("b3fc0a57-556c-4046-abd9-a98e8ff02b2a"), "dtnp@gmail.com", "123456", "Member" },
                    { new Guid("15874b96-f56c-4eda-ab58-0e8b3c07716f"), "mm@gmail.com", "123456", "Member" }
                });
        }
    }
}
