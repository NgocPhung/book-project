﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace BookManagement.Models.Migrations
{
    public partial class AlterRequestTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Fundses",
                keyColumn: "FundsId",
                keyValue: new Guid("19538515-1b8d-4b98-9212-8a909e906f15"));

            migrationBuilder.DeleteData(
                table: "Fundses",
                keyColumn: "FundsId",
                keyValue: new Guid("4c205297-b2d7-4f82-9fc4-06f1eb574326"));

            migrationBuilder.DeleteData(
                table: "Fundses",
                keyColumn: "FundsId",
                keyValue: new Guid("68c43e91-ea3f-4b91-9703-546b818d95ed"));

            migrationBuilder.DeleteData(
                table: "Fundses",
                keyColumn: "FundsId",
                keyValue: new Guid("6ca73a92-6824-4160-8a4e-ccb3af2e187c"));

            migrationBuilder.DeleteData(
                table: "Requests",
                keyColumn: "RequestId",
                keyValue: new Guid("22d3f430-eeac-44a8-bd0c-517ade2fb0db"));

            migrationBuilder.DeleteData(
                table: "Requests",
                keyColumn: "RequestId",
                keyValue: new Guid("686d4294-152a-4c60-806c-ebc870504454"));

            migrationBuilder.DeleteData(
                table: "Requests",
                keyColumn: "RequestId",
                keyValue: new Guid("6a1a6f50-aefb-4d51-8735-77e226e44a86"));

            migrationBuilder.DeleteData(
                table: "Requests",
                keyColumn: "RequestId",
                keyValue: new Guid("b540bdeb-c548-4a78-b22d-972251bce6ef"));

            migrationBuilder.DeleteData(
                table: "Requests",
                keyColumn: "RequestId",
                keyValue: new Guid("e926bf69-c818-4138-8cb7-d9ca25852b19"));

            migrationBuilder.DeleteData(
                table: "Requests",
                keyColumn: "RequestId",
                keyValue: new Guid("fbeaf197-7389-4028-a1c6-7b997a88f331"));

            migrationBuilder.AddColumn<bool>(
                name: "IsApprove",
                table: "Requests",
                nullable: false,
                defaultValue: false);

            migrationBuilder.InsertData(
                table: "Fundses",
                columns: new[] { "FundsId", "Cost", "Income", "Total" },
                values: new object[,]
                {
                    { new Guid("a8f0b886-e313-4b64-9855-d36c7a0eb772"), 12.0, 13.0, 123.0 },
                    { new Guid("fbe24dae-59f6-4ce7-b297-b62ac0c2935c"), 11.0, 11.0, 111.0 },
                    { new Guid("da7acd54-5f82-4263-89a4-b09d43897f07"), 9.0, 28.0, 1985.0 },
                    { new Guid("b13d9a58-cb50-431c-94c4-612eae1fb364"), 12.0, 13.0, 222.0 }
                });

            migrationBuilder.InsertData(
                table: "Requests",
                columns: new[] { "RequestId", "AdminId", "BookId", "Content", "IsApprove", "MemberId" },
                values: new object[,]
                {
                    { new Guid("83dc877d-a30e-4459-bc3f-beb26818cfb5"), new Guid("9682aaf5-bf44-4b27-a2c9-66ed17e63c13"), new Guid("4d9884a9-4017-44bf-9003-38c28b097a29"), "Content1", false, new Guid("b6f8141a-dd69-4a96-b8c9-34d9748ca39a") },
                    { new Guid("51a14e90-0a41-4154-93f0-e88eebe43d91"), new Guid("4d7728db-3166-47d1-a945-189a47c152bc"), new Guid("b765e5c2-a225-4797-a871-ce861addbb9b"), "Content2", false, new Guid("28785aa9-2a01-4681-b561-4536b94c6f56") },
                    { new Guid("bae62879-7821-495f-932e-ae22bcd575ea"), new Guid("23a97781-640b-434e-aa89-326a2c5f3e59"), new Guid("93bf23eb-73ec-4be6-9eb1-fbf94cd825fb"), "Content3", false, new Guid("0f1beee3-a6c5-482c-87bd-b80507addc9e") },
                    { new Guid("83e915fd-b098-45d2-9660-9de57f1107b3"), new Guid("e260774c-cfd4-48d4-9238-67b2f3271ebe"), new Guid("fa75df69-38b0-4687-8d00-e8b1b5984f0e"), "Content4", false, new Guid("29a5c873-b028-48d6-a263-441673e12839") },
                    { new Guid("9dc1a25d-7c42-4e60-921b-bf6ec2be884e"), new Guid("2daf87e3-9b99-4a41-921f-d9f2079ede06"), new Guid("6b2239c7-8e36-4276-b4fb-c20fd9c65463"), "Content5", false, new Guid("75992935-a077-4f13-b682-021532690134") },
                    { new Guid("5cefa264-2618-4ae9-a69f-fdb27bb08aa2"), new Guid("118fc55d-5b19-42e9-b107-d92c060daaef"), new Guid("e28e96a3-3146-48b8-a1ba-469e5a380cc2"), "Content6", false, new Guid("951873d3-56d7-4b57-96f9-23edffc027cc") }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Fundses",
                keyColumn: "FundsId",
                keyValue: new Guid("a8f0b886-e313-4b64-9855-d36c7a0eb772"));

            migrationBuilder.DeleteData(
                table: "Fundses",
                keyColumn: "FundsId",
                keyValue: new Guid("b13d9a58-cb50-431c-94c4-612eae1fb364"));

            migrationBuilder.DeleteData(
                table: "Fundses",
                keyColumn: "FundsId",
                keyValue: new Guid("da7acd54-5f82-4263-89a4-b09d43897f07"));

            migrationBuilder.DeleteData(
                table: "Fundses",
                keyColumn: "FundsId",
                keyValue: new Guid("fbe24dae-59f6-4ce7-b297-b62ac0c2935c"));

            migrationBuilder.DeleteData(
                table: "Requests",
                keyColumn: "RequestId",
                keyValue: new Guid("51a14e90-0a41-4154-93f0-e88eebe43d91"));

            migrationBuilder.DeleteData(
                table: "Requests",
                keyColumn: "RequestId",
                keyValue: new Guid("5cefa264-2618-4ae9-a69f-fdb27bb08aa2"));

            migrationBuilder.DeleteData(
                table: "Requests",
                keyColumn: "RequestId",
                keyValue: new Guid("83dc877d-a30e-4459-bc3f-beb26818cfb5"));

            migrationBuilder.DeleteData(
                table: "Requests",
                keyColumn: "RequestId",
                keyValue: new Guid("83e915fd-b098-45d2-9660-9de57f1107b3"));

            migrationBuilder.DeleteData(
                table: "Requests",
                keyColumn: "RequestId",
                keyValue: new Guid("9dc1a25d-7c42-4e60-921b-bf6ec2be884e"));

            migrationBuilder.DeleteData(
                table: "Requests",
                keyColumn: "RequestId",
                keyValue: new Guid("bae62879-7821-495f-932e-ae22bcd575ea"));

            migrationBuilder.DropColumn(
                name: "IsApprove",
                table: "Requests");

            migrationBuilder.InsertData(
                table: "Fundses",
                columns: new[] { "FundsId", "Cost", "Income", "Total" },
                values: new object[,]
                {
                    { new Guid("68c43e91-ea3f-4b91-9703-546b818d95ed"), 12.0, 13.0, 123.0 },
                    { new Guid("4c205297-b2d7-4f82-9fc4-06f1eb574326"), 11.0, 11.0, 111.0 },
                    { new Guid("6ca73a92-6824-4160-8a4e-ccb3af2e187c"), 9.0, 28.0, 1985.0 },
                    { new Guid("19538515-1b8d-4b98-9212-8a909e906f15"), 12.0, 13.0, 222.0 }
                });

            migrationBuilder.InsertData(
                table: "Requests",
                columns: new[] { "RequestId", "AdminId", "BookId", "Content", "MemberId" },
                values: new object[,]
                {
                    { new Guid("fbeaf197-7389-4028-a1c6-7b997a88f331"), new Guid("9a09ac81-bd1b-4a0a-8e09-9e42d306c55a"), new Guid("ca51dcf8-475b-44e2-a4b6-30494015a70c"), "Content1", new Guid("6ce6719a-feab-4505-b8f1-7c9c46f8f883") },
                    { new Guid("b540bdeb-c548-4a78-b22d-972251bce6ef"), new Guid("d3983e8e-d587-4429-bf4e-499f2ee21a7e"), new Guid("22fc4884-b7e5-40ee-9869-73c9993658a9"), "Content2", new Guid("86110ebe-0964-4fdd-982c-d410890fae15") },
                    { new Guid("686d4294-152a-4c60-806c-ebc870504454"), new Guid("e7e93570-58d9-4acd-887d-5aaf634ee0ab"), new Guid("5c41c355-6b1f-461e-acd9-979ba19811c6"), "Content3", new Guid("28fed54e-03dc-4ee0-a2a6-e236627cdd46") },
                    { new Guid("22d3f430-eeac-44a8-bd0c-517ade2fb0db"), new Guid("d3aadf63-56d0-495e-b6aa-0ea15d1ce539"), new Guid("017d7b94-298e-4f72-ba71-849c8c52b5cf"), "Content4", new Guid("49995da1-0f4e-4106-af88-27922a4af132") },
                    { new Guid("e926bf69-c818-4138-8cb7-d9ca25852b19"), new Guid("759ae02d-26fb-45c7-b24b-04b458759981"), new Guid("ea9e875c-0fbe-44b4-83d6-de1c24898ca9"), "Content5", new Guid("b3d8dea5-f75a-4be5-ba68-6708150a7a0b") },
                    { new Guid("6a1a6f50-aefb-4d51-8735-77e226e44a86"), new Guid("ddb04b03-cc52-456c-9a31-8b3021f1a468"), new Guid("622658a4-eebd-4450-8391-12fc1d7636c7"), "Content6", new Guid("a3651eaf-8d47-4053-9f62-58558d415f70") }
                });
        }
    }
}
