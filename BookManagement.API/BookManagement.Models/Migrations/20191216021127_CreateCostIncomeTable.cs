﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace BookManagement.Models.Migrations
{
    public partial class CreateCostIncomeTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Fundses",
                keyColumn: "FundsId",
                keyValue: new Guid("50d951bc-d2b3-4a08-948b-5b612de620a3"));

            migrationBuilder.DeleteData(
                table: "Fundses",
                keyColumn: "FundsId",
                keyValue: new Guid("7f58fefd-8c6c-4386-8174-888321c2b8fa"));

            migrationBuilder.DeleteData(
                table: "Fundses",
                keyColumn: "FundsId",
                keyValue: new Guid("a470daa7-125b-445f-886f-8b3cfcb52dff"));

            migrationBuilder.DeleteData(
                table: "Fundses",
                keyColumn: "FundsId",
                keyValue: new Guid("fdbbce0a-7204-4ae1-a30c-eb3f87f0b008"));

            migrationBuilder.DeleteData(
                table: "Requests",
                keyColumn: "RequestId",
                keyValue: new Guid("0df40072-b830-4bf8-a26a-d1bab2b7254b"));

            migrationBuilder.DeleteData(
                table: "Requests",
                keyColumn: "RequestId",
                keyValue: new Guid("12c6bcae-682a-4c5a-adb9-ba81237ead9f"));

            migrationBuilder.DeleteData(
                table: "Requests",
                keyColumn: "RequestId",
                keyValue: new Guid("3164af22-b76e-4b70-9ac8-5b927bf57fe0"));

            migrationBuilder.DeleteData(
                table: "Requests",
                keyColumn: "RequestId",
                keyValue: new Guid("95ffda47-0874-4ad7-81ec-e87d99d9ff4f"));

            migrationBuilder.DeleteData(
                table: "Requests",
                keyColumn: "RequestId",
                keyValue: new Guid("a020bdd0-57b4-4aff-8ee9-9de9de5fd66f"));

            migrationBuilder.DeleteData(
                table: "Requests",
                keyColumn: "RequestId",
                keyValue: new Guid("e295ff6f-9902-45e2-8b47-6dc6c5619b02"));

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "UserId",
                keyValue: new Guid("0faf6551-f027-487d-82df-61f3d254c38e"));

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "UserId",
                keyValue: new Guid("731b6859-2b6e-4f0c-b1d4-19e2bab8cf2e"));

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "UserId",
                keyValue: new Guid("92e5d33b-e2ca-422d-a1cf-75b3feb4de26"));

            migrationBuilder.CreateTable(
                name: "Costs",
                columns: table => new
                {
                    CostId = table.Column<Guid>(nullable: false),
                    CostMoney = table.Column<double>(nullable: false),
                    Content = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Costs", x => x.CostId);
                });

            migrationBuilder.CreateTable(
                name: "Incomes",
                columns: table => new
                {
                    IncomeId = table.Column<Guid>(nullable: false),
                    IncometMoney = table.Column<double>(nullable: false),
                    Content = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Incomes", x => x.IncomeId);
                });

            migrationBuilder.InsertData(
                table: "Fundses",
                columns: new[] { "FundsId", "Content", "Cost", "Income", "Total" },
                values: new object[,]
                {
                    { new Guid("e5df01ee-a6a0-4150-96b4-9b0357b7abc8"), "a", 12.0, 13.0, 123.0 },
                    { new Guid("4333b4bf-38e9-4be5-a694-5e2cfc3f39d4"), "b", 11.0, 11.0, 111.0 },
                    { new Guid("61e59302-2735-4471-bf2d-dba47414231a"), "c", 9.0, 28.0, 1985.0 },
                    { new Guid("ec34699b-fc85-4bc5-95fa-4491ee72b7af"), "d", 12.0, 13.0, 222.0 }
                });

            migrationBuilder.InsertData(
                table: "Requests",
                columns: new[] { "RequestId", "AdminId", "BookId", "Content", "IsApprove", "MemberId" },
                values: new object[,]
                {
                    { new Guid("18eea785-a34b-465b-a4fa-901044d9879b"), new Guid("3052fa9b-fca8-4181-9a31-883912c16448"), new Guid("e69923a8-c218-4621-bc96-52ca8fa79682"), "Content1", false, new Guid("d7cd9d08-20c9-4dcb-acc2-bf1120872b29") },
                    { new Guid("9bff9d13-d8e6-403d-b996-ec5922de53d5"), new Guid("bfbdc176-a724-42a2-a575-03f84e8481d2"), new Guid("bb7ec5c6-0cb4-4f7e-b0fa-ea4ec188c72d"), "Content2", false, new Guid("e5698645-bd52-4cb1-9e6f-f03ff997b177") },
                    { new Guid("1903aab2-cf91-4955-90ee-3210867d973d"), new Guid("7e89bfd4-530a-493f-9e02-bd7061d35939"), new Guid("fc67e1b7-b7b8-4d0b-b8dc-667924c6077c"), "Content3", false, new Guid("89323bf2-1e0e-482f-8fa6-8970180dc78d") },
                    { new Guid("13c5aec6-8e2b-424d-8aeb-1337bf722da6"), new Guid("e0f122ea-9f5e-4f1c-9ea2-a2447770d212"), new Guid("8ca147a0-a630-4e7a-91ac-dc71cdff9a50"), "Content4", false, new Guid("47b8d055-898d-4cad-ae99-2ff4dc325299") },
                    { new Guid("c6f47758-a62d-40a2-ab7d-94a28d4aa972"), new Guid("37f727f1-cbdb-448e-8dd0-01340a01276a"), new Guid("f81906b0-3795-4427-9213-0e84c16c1c8f"), "Content5", false, new Guid("0701e539-a23e-4a7f-9c0e-3eca0003881c") },
                    { new Guid("431442eb-9ef3-4ee8-ae32-32d0462c8de0"), new Guid("0aaccf01-8c2d-4e92-b102-bbed205e5788"), new Guid("b5bb5248-e327-4fe7-90af-f460c1101464"), "Content6", false, new Guid("4a90a888-676e-4671-8333-0712ad5e0882") }
                });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "UserId", "Email", "PassWord", "Role" },
                values: new object[,]
                {
                    { new Guid("7014e07d-aee1-4691-a977-9bbb54fd5a3c"), "abc@gmail.com", "123456", "Admin" },
                    { new Guid("b3fc0a57-556c-4046-abd9-a98e8ff02b2a"), "dtnp@gmail.com", "123456", "Member" },
                    { new Guid("15874b96-f56c-4eda-ab58-0e8b3c07716f"), "mm@gmail.com", "123456", "Member" }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Costs");

            migrationBuilder.DropTable(
                name: "Incomes");

            migrationBuilder.DeleteData(
                table: "Fundses",
                keyColumn: "FundsId",
                keyValue: new Guid("4333b4bf-38e9-4be5-a694-5e2cfc3f39d4"));

            migrationBuilder.DeleteData(
                table: "Fundses",
                keyColumn: "FundsId",
                keyValue: new Guid("61e59302-2735-4471-bf2d-dba47414231a"));

            migrationBuilder.DeleteData(
                table: "Fundses",
                keyColumn: "FundsId",
                keyValue: new Guid("e5df01ee-a6a0-4150-96b4-9b0357b7abc8"));

            migrationBuilder.DeleteData(
                table: "Fundses",
                keyColumn: "FundsId",
                keyValue: new Guid("ec34699b-fc85-4bc5-95fa-4491ee72b7af"));

            migrationBuilder.DeleteData(
                table: "Requests",
                keyColumn: "RequestId",
                keyValue: new Guid("13c5aec6-8e2b-424d-8aeb-1337bf722da6"));

            migrationBuilder.DeleteData(
                table: "Requests",
                keyColumn: "RequestId",
                keyValue: new Guid("18eea785-a34b-465b-a4fa-901044d9879b"));

            migrationBuilder.DeleteData(
                table: "Requests",
                keyColumn: "RequestId",
                keyValue: new Guid("1903aab2-cf91-4955-90ee-3210867d973d"));

            migrationBuilder.DeleteData(
                table: "Requests",
                keyColumn: "RequestId",
                keyValue: new Guid("431442eb-9ef3-4ee8-ae32-32d0462c8de0"));

            migrationBuilder.DeleteData(
                table: "Requests",
                keyColumn: "RequestId",
                keyValue: new Guid("9bff9d13-d8e6-403d-b996-ec5922de53d5"));

            migrationBuilder.DeleteData(
                table: "Requests",
                keyColumn: "RequestId",
                keyValue: new Guid("c6f47758-a62d-40a2-ab7d-94a28d4aa972"));

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "UserId",
                keyValue: new Guid("15874b96-f56c-4eda-ab58-0e8b3c07716f"));

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "UserId",
                keyValue: new Guid("7014e07d-aee1-4691-a977-9bbb54fd5a3c"));

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "UserId",
                keyValue: new Guid("b3fc0a57-556c-4046-abd9-a98e8ff02b2a"));

            migrationBuilder.InsertData(
                table: "Fundses",
                columns: new[] { "FundsId", "Content", "Cost", "Income", "Total" },
                values: new object[,]
                {
                    { new Guid("50d951bc-d2b3-4a08-948b-5b612de620a3"), "a", 12.0, 13.0, 123.0 },
                    { new Guid("7f58fefd-8c6c-4386-8174-888321c2b8fa"), "b", 11.0, 11.0, 111.0 },
                    { new Guid("a470daa7-125b-445f-886f-8b3cfcb52dff"), "c", 9.0, 28.0, 1985.0 },
                    { new Guid("fdbbce0a-7204-4ae1-a30c-eb3f87f0b008"), "d", 12.0, 13.0, 222.0 }
                });

            migrationBuilder.InsertData(
                table: "Requests",
                columns: new[] { "RequestId", "AdminId", "BookId", "Content", "IsApprove", "MemberId" },
                values: new object[,]
                {
                    { new Guid("e295ff6f-9902-45e2-8b47-6dc6c5619b02"), new Guid("651ea34d-2e88-4220-8a72-099d983a6733"), new Guid("c91c6472-584c-41fb-90ad-1cb0c4995928"), "Content1", false, new Guid("94507887-5c51-4c52-823d-3883fffa4cfc") },
                    { new Guid("95ffda47-0874-4ad7-81ec-e87d99d9ff4f"), new Guid("6e7c0590-f243-405b-9f38-1cc15059311d"), new Guid("75158715-fc7b-4c19-b4b1-e1d7e7b503d3"), "Content2", false, new Guid("dcf9dcd7-363b-46af-816a-cdc8386eccd3") },
                    { new Guid("12c6bcae-682a-4c5a-adb9-ba81237ead9f"), new Guid("1e6730f6-5bdc-49e5-8b4e-c46502fbb16b"), new Guid("176f5e87-cf14-4d0d-9a5c-9e5ee049934f"), "Content3", false, new Guid("e12dca7e-d6bc-42a5-8564-71547527bf02") },
                    { new Guid("a020bdd0-57b4-4aff-8ee9-9de9de5fd66f"), new Guid("8942cea7-ccbb-47f5-a37c-74cc504330aa"), new Guid("8538e002-887d-4661-8796-09656f36b984"), "Content4", false, new Guid("24ffed13-a0f1-48ed-9f21-7c98dd252bd6") },
                    { new Guid("0df40072-b830-4bf8-a26a-d1bab2b7254b"), new Guid("69780012-88a7-4673-a216-d86ce6f3f695"), new Guid("f1e07559-df74-4d7d-ba12-70d3936f4d84"), "Content5", false, new Guid("b30d3178-8733-497b-9d3a-f0c060f1f7ea") },
                    { new Guid("3164af22-b76e-4b70-9ac8-5b927bf57fe0"), new Guid("73714595-c5eb-4759-9179-26b92ef670d6"), new Guid("f91e3558-12e8-4d12-91ba-f64dcc12b56c"), "Content6", false, new Guid("5625a928-6cb7-4a4e-acb7-7729fa3879a7") }
                });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "UserId", "Email", "PassWord", "Role" },
                values: new object[,]
                {
                    { new Guid("0faf6551-f027-487d-82df-61f3d254c38e"), "abc@gmail.com", "123456", "Admin" },
                    { new Guid("92e5d33b-e2ca-422d-a1cf-75b3feb4de26"), "dtnp@gmail.com", "123456", "Member" },
                    { new Guid("731b6859-2b6e-4f0c-b1d4-19e2bab8cf2e"), "mm@gmail.com", "123456", "Member" }
                });
        }
    }
}
