﻿using System;
using System.Linq;
using BookManagement.Models.Data;
using BookManagement.Models.Models;
using BookManagement.Service.Services.Interface;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting.Internal;
using Microsoft.AspNetCore.Mvc;

namespace BookManagement.API.Controllers
{    
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class FundController : Controller
    {
        private IFundService _fundService;
        private BookDbContext _context;

        public FundController(IFundService fundService, BookDbContext context)
        {
            _fundService = fundService;
            _context = context;
        }
        // GET
        [HttpGet(Name = "Total")]
        public IActionResult GetTotal()
        {
            try
            {
                var response = _fundService.Fund();
                return Ok(response);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        
        
    }
}