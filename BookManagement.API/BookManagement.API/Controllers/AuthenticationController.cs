﻿using System;
using BookManagement.API.JwtProvider;
using BookManagement.Models.Common;
using BookManagement.Models.Models;
using BookManagement.Service.Services.Interface;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace BookManagement.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthenticationController : Controller
    {
        private IJwtProvider _jwtProvider;

        public AuthenticationController(IJwtProvider jwtProvider)
        {
            _jwtProvider = jwtProvider;
        }
        
        [HttpPost, Route("login")]
        public IActionResult Login([FromBody] User model)
        {
            try
            {
                //Lay user ra dua vao thong tin dang nhap

                //Day la demo
                var user = new User
                {
                    UserId = Guid.Parse("731B6859-2B6E-4F0C-B1D4-19E2BAB8CF2E"),
                    Email = "mm@gmail.com",
                    PassWord = "123456",
                    Role = Constant.ROLE_NAME.ADMIN
                };
                var token = _jwtProvider.GenerateJwtToken(user);
                return Ok(token);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}