﻿using System;
using BookManagement.Service.Services.Implement;
using BookManagement.Service.Services.Interface;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace BookManagement.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class IncomeController : Controller
    {
        private IIncomeService _incomeService;

        public IncomeController(IIncomeService incomeService)
        {
            _incomeService = incomeService;
        }
        // GET
        [HttpGet("{pageSize}/{pageNumber})")]
        public IActionResult Gets(int pageSize=10, int pageNumber=1)
        {
            var response = _incomeService.GetsPagging(pageSize, pageNumber);
            return Ok(response);
        }

        [HttpGet("TotalByMonth/{month}/{year})")]
        public IActionResult TotalIncomeByMonth(int month, int year)
        {
            try
            { var incomeByMonth = _incomeService.TotalIncomeByMonth(month, year);
                return Ok(incomeByMonth);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        
        [HttpGet("TotalByYear/{year})")]
        public IActionResult TotalIncomeByYear(int year)
        {
            try
            {
                var incomeByYear = _incomeService.TotalIncomeByYear( year);
                return Ok(incomeByYear);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        
        [HttpGet]
        public IActionResult TotalIncome()
        {
            try
            {
                var income = _incomeService.TotalIncome();
                return Ok(income);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}