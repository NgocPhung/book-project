﻿using System;
using System.Linq;
using BookManagement.Models.Common;
using BookManagement.Models.Data;
using BookManagement.Models.Models;
using BookManagement.Service.Services.Interface;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace BookManagement.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    //[Authorize]
    public class CostController : Controller
    {
        private ICostService _costService;

        public CostController(ICostService costService)
        {
            _costService = costService;
        }

        // GET
        [Authorize(Roles = Constant.ROLE_NAME.ADMIN +","+ Constant.ROLE_NAME.MEMBER)]
        [HttpGet("{pageSize}/{pageNumber}")]
        public IActionResult Gets(int pageSize=10, int pageNumber=1)
        {
            var response = _costService.GetsPagging(pageSize, pageNumber);
            return Ok(response);
        }

        [Authorize(Roles = Constant.ROLE_NAME.ADMIN)]
        [HttpPost]
        public IActionResult Create(Cost cost)
        {
            _costService.Add(cost);
            return Ok();
        }
        
        [Authorize(Roles = Constant.ROLE_NAME.ADMIN)]
        [HttpGet("TotalByMonth/{month}/{year}")]
        public IActionResult TotalCostByMonth(int month , int year )
        {
            try
            {
                var costByMonth = _costService.TotalCostByMonth(month, year);
                return Ok(costByMonth);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
       
        [Authorize(Roles = Constant.ROLE_NAME.ADMIN)]
        [HttpGet("TotalByYear/{year}")]
        public IActionResult TotalCostByYear(int year)
        {
            try
            {
                var costByYear = _costService.TotalCostByYear( year);
                return Ok(costByYear);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        
        [Authorize(Roles = Constant.ROLE_NAME.ADMIN)]
        [HttpGet("TotalCost")]        
        public IActionResult TotalCost()
        {
            try
            {
                var cost = _costService.TotalCost();
                return Ok(cost);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        
        [Authorize(Roles = Constant.ROLE_NAME.ADMIN +","+ Constant.ROLE_NAME.MEMBER)]
        [HttpGet("StatisticCost/{month}/{year}")]        
        public IActionResult StatisticCostByMonth(int month, int year)
        {
            try
            {
                var listCost = _costService.StatisticCostsByMonth(month, year);
                return Ok(listCost);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}