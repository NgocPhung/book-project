﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using BookManagement.Models.Common;
using BookManagement.Models.Models;
using BookManagement.Service.Services.Interface;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace BookManagement.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize] //Phai login thanh cong
    public class RequestController : Controller
    {
        private IRequestService _requestService;

        public RequestController(IRequestService requestService)
        {
            _requestService = requestService;
        }

        // GET
        [HttpGet("{pageSize}/{pageNumber}")]
        public IActionResult Gets(int pageSize = 10, int pageNumber = 1)
        {
            var response = _requestService.GetsPagging(pageSize, pageNumber);
            return Ok(response);
        }

        [Authorize(Roles = Constant.ROLE_NAME.ADMIN)] //Phan quyen chi cho Admin
        [HttpGet("{id}")]
        public IActionResult Get(Guid id)
        {
            try
            {
                var response = _requestService.GetById(id);
                if (response == null)
                    return NotFound();
                return Ok(response);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        //Phan quyen chuc nang cho Member
        [Authorize(Roles = Constant.ROLE_NAME.MEMBER)]
        [HttpPost]
        public IActionResult Create(Request request)
        {
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            var claims = new List<Claim>();
            // Lấy dữ liệu claims
            if (identity != null)
            {
                claims = identity.Claims.ToList();
            }
            
            //Truy xuất các dữ liệu của claims -- Khi khoi tao token co them du lieu vao Claims
            var userIdClaim = claims.FirstOrDefault(e => e.Type == "UserId");
            var userId = userIdClaim != null ? userIdClaim.Value : "";


            //MemberId
            //userId khi lay ra tu Claim, kieu du lieu la string
            //de chuyen qua kieu du lieu khac: <TenKieuDuLieu>.Parse(<KieuDuLieuString>)
            request.MemberId = Guid.Parse(userId);

            //AdminId
            request.AdminId = Guid.Parse(Constant.ADMIN_ID);
            
            _requestService.Add(request);
            return Ok();
        }

        //Phan quyen chuc nang cho Admin va Member        
        [HttpPut]
        public IActionResult Edit(Request request)
        {
            try
            {
                var r = _requestService.GetById(request.RequestId);
                if (r == null)
                    return NotFound();

                var identity = HttpContext.User.Identity as ClaimsIdentity;
                var claims = new List<Claim>();
                // Lấy dữ liệu claims
                if (identity != null)
                {
                    claims = identity.Claims.ToList();
                }

                //Lay role de kiem tra Admin hay Member
                var roleClaim = claims.FirstOrDefault(e => e.Type == ClaimTypes.Role);
                var userIdClaim = claims.FirstOrDefault(e => e.Type == "UserId");

                var roleId = roleClaim != null ? roleClaim.Value : Constant.ROLE_NAME.MEMBER;
                var userId = userIdClaim != null ? userIdClaim.Value : "";

                //Kiem tra quyen User
                //Neu la ADMIN
                if (roleId == Constant.ROLE_NAME.ADMIN)
                    _requestService.Update(request);
                else if(Guid.Parse(userId) == request.MemberId)
                {
                    //Neu la Member
                    //Chi update truong Content, BookId
                    r.Content = request.Content;
                    r.BookId = request.BookId;
                    _requestService.Update(r);
                }
                else
                {
                    return Unauthorized();
                }

                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [Authorize(Roles = Constant.ROLE_NAME.ADMIN)] //Phan quyen chi cho Admin
        [HttpDelete("{id}")]
        public IActionResult Delete(Guid id)
        {
            _requestService.Delete(id);
            return Ok();
        }
    }
}