﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using BookManagement.Models.Models;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;

namespace BookManagement.API.JwtProvider
{
    public interface IJwtProvider
    {
        string GenerateJwtToken(User user);
    }

    public class JwtProvider : IJwtProvider
    {
        private IConfiguration _configuration;

        public JwtProvider(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public string GenerateJwtToken(User user)
        {
            //Need to add Microsoft.Extensions.Configuration.Binder to use Get() function theo cấu trúc JwtConfig model
            //Đọc thông tin cấu hình JWT từ appsetting
            var tokenSettings = _configuration.GetSection("JwtToken").Get<TokenManagement>();

            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(tokenSettings.Secret));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
            var expires = DateTime.Now.AddMinutes(Convert.ToDouble(tokenSettings.AccessExpiration));

            //Create new claims or get from database
            IList<Claim> claims = new List<Claim>
            {
                //Tùy thuộc các trường dữ liệu của User, ví dụ đây User có các trường UserId, Email, Password và RoleId, ...
                new Claim("UserId", user.UserId.ToString()), //Dung Type=UserId de lay gia tri UserId da luu
                new Claim("Email", user.Email),
                new Claim("Password", user.PassWord),
                new Claim(ClaimTypes.Role, user.Role)
            };

            var jwtTokenHandler = new JwtSecurityTokenHandler();

            var token = new JwtSecurityToken(
                issuer: tokenSettings.Audience,
                audience: tokenSettings.Audience,
                claims: claims,
                notBefore: DateTime.UtcNow,
                expires: expires,
                signingCredentials: creds
            );

            return jwtTokenHandler.WriteToken(token);
        }
    }
}