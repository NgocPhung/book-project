﻿using System;
using System.IdentityModel.Tokens.Jwt;
using System.Text;
using BookManagement.Models.Models;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;

namespace BookManagement.API.JwtConfigure
{
    public static class JwtConfigure
{
        //Khai báo config sử dụng ở Startup.cs
        public static void Configure(IServiceCollection services, IConfiguration configuration)
        {
            services.Configure<TokenManagement>(configuration.GetSection("JwtToken"));

            // ===== Add Jwt Authentication ========
            JwtSecurityTokenHandler.DefaultInboundClaimTypeMap.Clear(); // remove default claims

            // configure Jwt
            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            })
            .AddJwtBearer(cfg =>
            {
                cfg.RequireHttpsMetadata = false;
                cfg.SaveToken = true;
                //cfg.ClaimsIssuer            = tokenSettings.Issuer;
                cfg.TokenValidationParameters = GetTokenValidationParameters(configuration);
                
            });
        }

        //Khai báo Token Validation Parameters sử dụng trong .AddJwtBearer ở hàm Configure
        private static TokenValidationParameters GetTokenValidationParameters(IConfiguration configuration)
        {
            var tokenSettings = configuration.GetSection("JwtToken").Get<TokenManagement>();

            return new TokenValidationParameters()
            {
                ValidateIssuer = true,
                ValidIssuer = tokenSettings.Issuer,

                ValidateIssuerSigningKey = true,
                IssuerSigningKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(tokenSettings.Secret)),

                ValidateAudience = true,
                ValidAudience = tokenSettings.Issuer,

                RequireExpirationTime = true,
                ValidateLifetime = true,
                ClockSkew = TimeSpan.Zero, // remove delay of token when expire
            };
        }
}
}