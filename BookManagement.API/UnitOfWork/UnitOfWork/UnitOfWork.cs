﻿using System;
using BookManagement.Models.Data;
using BookManagement.Models.Models;
using BookManagement.Repository.Repositories.Implement;
using BookManagement.Repository.Repositories.Interface;

namespace UnitOfWork.UnitOfWork
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly BookDbContext _context;

        public UnitOfWork(BookDbContext context)
        {
            _context = context;
            InitReponsitory();
        }
        private bool _disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }

            _disposed = true;
        }
        
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public IGenericRepository<Request> RequestRepository { get; set; }
        public IGenericRepository<Funds> FundRepository { get; set; }
        public IGenericRepository<Cost> CostRepository { get; set; }
        public IGenericRepository<Income> IncomeRepository { get; set; }

        private void InitReponsitory()
        {
            FundRepository = new GenericRepository<Funds>(_context);
            RequestRepository = new GenericRepository<Request>(_context);
            CostRepository = new GenericRepository<Cost>(_context);
            IncomeRepository = new GenericRepository<Income>(_context);
        }
        
        public void Save()
        {
            _context.SaveChanges();
        }
    }
}