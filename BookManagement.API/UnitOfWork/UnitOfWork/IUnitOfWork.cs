﻿using System;
using BookManagement.Models.Models;
using BookManagement.Repository.Repositories.Interface;

namespace UnitOfWork.UnitOfWork
{
    public interface IUnitOfWork : IDisposable
    {
        IGenericRepository<Request> RequestRepository { get; }
        IGenericRepository<Funds> FundRepository { get; }
        IGenericRepository<Cost> CostRepository { get; }
        IGenericRepository<Income> IncomeRepository { get; }
        void Save();
    }
}