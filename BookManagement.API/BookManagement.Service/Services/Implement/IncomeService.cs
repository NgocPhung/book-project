﻿using System;
using System.Linq;
using BookManagement.Models.Data;
using BookManagement.Models.Models;
using BookManagement.Service.Services.Interface;
using UnitOfWork.UnitOfWork;

namespace BookManagement.Service.Services.Implement
{
    public class IncomeService : IIncomeService 
    {
        private IUnitOfWork _unitOfWork;
        private BookDbContext _context;

        public IncomeService(IUnitOfWork unitOfWork, BookDbContext context)
        {
            _unitOfWork = unitOfWork;
            _context = context;
        }
        public PageList<Income> GetsPagging(int pageSize, int pageNumber)
        {
            return _unitOfWork.IncomeRepository.GetsPagging(pageSize, pageNumber);
        }

        public Income GetById(Guid id)
        {
            return _unitOfWork.IncomeRepository.GetById(id);
        }

        public double TotalIncomeByMonth(int month, int year)
        {
            var incomeByMonth = _context.Incomes.Where(c => c.CreatedDate.Month == month && c.CreatedDate.Year == year)
                .ToList().Sum(m => m.IncometMoney);
            return incomeByMonth;
        }

        public double TotalIncomeByYear(int year)
        {
            var incomeTotalByYear = _context.Incomes.Where(c => c.CreatedDate.Year == year).ToList()
                .Sum(m => m.IncometMoney);
            return incomeTotalByYear;
        }

        public double TotalIncome()
        {
            var income = _context.Incomes.ToList().Sum(m => m.IncometMoney);
            return income;
        }

        public void Add(Income income)
        {
            _unitOfWork.IncomeRepository.Add(income);
        }

        public void Update(Income income)
        {
            _unitOfWork.IncomeRepository.Update(income);
        }

        public void Delete(Guid id)
        {
            _unitOfWork.IncomeRepository.Delete(id);
        }

        public void Save()
        {
            _unitOfWork.IncomeRepository.Save();
        }
    }
}