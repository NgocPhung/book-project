﻿using System;
using BookManagement.Models.Models;
using BookManagement.Service.Services.Interface;
using UnitOfWork.UnitOfWork;

namespace BookManagement.Service.Services.Implement
{
    public class RequestService : IRequestService 
    {
        private readonly IUnitOfWork _unitOfWork;

        public RequestService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        public PageList<Request> GetsPagging(int pageSize, int pageNumber)
        {
            return _unitOfWork.RequestRepository.GetsPagging(pageSize, pageNumber);
        }

        public Request GetById(Guid id)
        {
            return _unitOfWork.RequestRepository.GetById(id);
        }

        public void Add(Request request)
        {
            _unitOfWork.RequestRepository.Add(request);
        }

        public void Update(Request request)
        {
            _unitOfWork.RequestRepository.Update(request);
        }

        public void Delete(Guid id)
        {
            _unitOfWork.RequestRepository.Delete(id);
        }

        public void Save()
        {
            _unitOfWork.RequestRepository.Save();
        }
    }
}