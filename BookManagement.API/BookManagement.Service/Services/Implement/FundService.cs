﻿using System;
using System.Linq;
using BookManagement.Models.Data;
using BookManagement.Models.Models;
using BookManagement.Service.Services.Interface;
using UnitOfWork.UnitOfWork;

namespace BookManagement.Service.Services.Implement
{
    public class FundService : IFundService
    {
        private IUnitOfWork _unitOfWork;
        private BookDbContext _context;
        private ICostService _costService;
        private IIncomeService _incomeService;

        public FundService(IUnitOfWork unitOfWork, BookDbContext context, 
            ICostService costService, IIncomeService incomeService)
        {
            _unitOfWork = unitOfWork;
            _context = context;
            _costService = costService;
            _incomeService = incomeService;
        }
        public PageList<Funds> GetsPagging(int pageSize, int pageNumber)
        {
            return _unitOfWork.FundRepository.GetsPagging(pageSize, pageNumber);
        }

        public Funds GetById(Guid id)
        {
            return _unitOfWork.FundRepository.GetById(id);
        }

        public void Add(Funds fund)
        {
            _unitOfWork.FundRepository.Add(fund);
        }

        public void Update(Funds fund)
        {
            _unitOfWork.FundRepository.Update(fund);
        }

        public void Delete(Guid id)
        {
            _unitOfWork.FundRepository.Delete(id);
        }

        public void Save()
        {
            _unitOfWork.FundRepository.Save();
        }

        public double Fund()
        {
            var incomeValue = _incomeService.TotalIncome();
            var costValue = _costService.TotalCost();

            return incomeValue - costValue;
        }
    }
}