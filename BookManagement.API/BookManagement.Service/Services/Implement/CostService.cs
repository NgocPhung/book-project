﻿using System;
using System.Collections.Generic;
using System.Linq;
using BookManagement.Models.Data;
using BookManagement.Models.Models;
using BookManagement.Service.Services.Interface;
using UnitOfWork.UnitOfWork;

namespace BookManagement.Service.Services.Implement
{
    public class CostService : ICostService
    {
        private readonly IUnitOfWork _unitOfWork;
        private BookDbContext _context;

        public CostService(IUnitOfWork unitOfWork, BookDbContext context)
        {
            _unitOfWork = unitOfWork;
            _context = context;
        }
        public PageList<Cost> GetsPagging(int pageSize, int pageNumber)
        {
            return _unitOfWork.CostRepository.GetsPagging(pageSize, pageNumber);
        }

        public double TotalCostByMonth(int month, int year)
        {
            var costByMonth =_context.Costs.Where(c => c.CreatedDate.Month == month && c.CreatedDate.Year == year).ToList()
                .Sum(m => m.CostMoney);
            return costByMonth;
        }

        public double TotalCostByYear(int year)
        {
            var costByYear =_context.Costs.Where(c => c.CreatedDate.Year == year).ToList()
                .Sum(m => m.CostMoney);
            return costByYear;
        }

        public double TotalCost()
        {
            var cost =_context.Costs
                .Sum(m => m.CostMoney);
            
            return cost;
        }

        public IEnumerable<Cost> StatisticCostsByMonth(int month, int year)
        {
            var listCost = _context.Costs.Where(c => c.CreatedDate.Month == month && c.CreatedDate.Year == year).ToList();
            return listCost;
        }

        public Cost GetById(Guid id)
        {
            return _unitOfWork.CostRepository.GetById(id);
        }

        public void Add(Cost cost)
        {
            _unitOfWork.CostRepository.Add(cost);
        }

        public void Update(Cost cost)
        {
            _unitOfWork.CostRepository.Update(cost);
        }

        public void Delete(Guid id)
        {
            _unitOfWork.CostRepository.Delete(id);
        }

        public void Save()
        {
            _unitOfWork.CostRepository.Save();
        }
    }
}