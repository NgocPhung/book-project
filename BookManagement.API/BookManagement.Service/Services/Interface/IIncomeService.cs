﻿using System;
using BookManagement.Models.Models;

namespace BookManagement.Service.Services.Interface
{
    public interface IIncomeService
    {
        PageList<Income> GetsPagging(int pageSize, int pageNumber);
        Income GetById(Guid id);
        double TotalIncomeByMonth(int month, int year);
        double TotalIncomeByYear(int year);
        double TotalIncome();
        void Add(Income income);
        void Update(Income income);
        void Delete(Guid id);
        void Save();
    }
}