﻿namespace BookManagement.Service.Services.Interface
{
    public interface IUserManagementService
    {
        bool IsValidUser(string username, string password);
    }
}