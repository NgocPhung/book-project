﻿using System;
using BookManagement.Models.Models;

namespace BookManagement.Service.Services.Interface
{
    public interface IRequestService
    {
        PageList<Request> GetsPagging(int pageSize, int pageNumber);
        Request GetById(Guid id);
        void Add(Request request);
        void Update(Request request);
        void Delete(Guid id);
        void Save();
    }
}