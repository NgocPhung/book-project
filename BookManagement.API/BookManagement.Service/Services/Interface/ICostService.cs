﻿using System;
using System.Collections.Generic;
using BookManagement.Models.Models;

namespace BookManagement.Service.Services.Interface
{
    public interface ICostService
    {
        PageList<Cost> GetsPagging(int pageSize, int pageNumber);
        double TotalCostByMonth(int month, int year);
        double TotalCostByYear(int year);
        double TotalCost();
        IEnumerable<Cost> StatisticCostsByMonth(int month, int year);
        void Add(Cost cost);
        void Update(Cost cost);
        void Delete(Guid id);
        void Save();
    }
}