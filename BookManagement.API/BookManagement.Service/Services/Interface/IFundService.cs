﻿using System;
using BookManagement.Models.Models;

namespace BookManagement.Service.Services.Interface
{
    public interface IFundService
    {
        PageList<Funds> GetsPagging(int pageSize, int pageNumber);
        Funds GetById(Guid id);
        void Add(Funds fund);
        void Update(Funds fund);
        void Delete(Guid id);
        void Save();
        double Fund();
    }
}