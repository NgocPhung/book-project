﻿using BookManagement.Models.Models;

namespace BookManagement.Repository.Repositories.Interface
{
    public interface IRequestRepository : IGenericRepository<Request>
    {
        
    }
}