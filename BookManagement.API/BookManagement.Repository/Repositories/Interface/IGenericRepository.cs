﻿using System;
using System.Collections.Generic;
using BookManagement.Models.Models;

namespace BookManagement.Repository.Repositories.Interface
{
    public interface IGenericRepository<TEntity>
    {
        PageList<TEntity> GetsPagging(int pageSize, int pageNumber);
        TEntity GetById(Guid id);
        void Add(TEntity entity);
        void Update(TEntity entity);
        void Delete(Guid id);
        void Save();
    }
}