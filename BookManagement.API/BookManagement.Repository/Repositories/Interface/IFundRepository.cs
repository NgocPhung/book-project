﻿using BookManagement.Models.Models;

namespace BookManagement.Repository.Repositories.Interface
{
    public interface IFundRepository : IGenericRepository<Funds>
    {
        
    }
}