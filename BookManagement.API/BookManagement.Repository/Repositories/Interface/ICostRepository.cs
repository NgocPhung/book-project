﻿using BookManagement.Models.Models;
using Microsoft.AspNetCore.Mvc;

namespace BookManagement.Repository.Repositories.Interface
{
    public interface ICostRepository : IGenericRepository<Cost>
    {
    }
}