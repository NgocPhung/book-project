﻿using System.Linq;
using System.Runtime.InteropServices.ComTypes;
using BookManagement.Models.Data;
using BookManagement.Models.Models;
using BookManagement.Repository.Repositories.Interface;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Remotion.Linq.Clauses;

namespace BookManagement.Repository.Repositories.Implement
{
    public class CostRepository : GenericRepository<Cost>, ICostRepository
    {
        public CostRepository(BookDbContext context) : base(context)
        {
        }

    }
}