﻿using System;
using System.Collections.Generic;
using BookManagement.Models.Data;
using BookManagement.Models.Models;
using BookManagement.Repository.Repositories.Interface;
using Microsoft.EntityFrameworkCore;

namespace BookManagement.Repository.Repositories.Implement
{
    public class GenericRepository<TEntity> : IGenericRepository<TEntity> where TEntity : class
    {
        protected BookDbContext _context;
        private DbSet<TEntity> _dbSet;

        public GenericRepository(BookDbContext context)
        {
            _context = context;
            _dbSet = _context.Set<TEntity>();
        }

        public PageList<TEntity> GetsPagging(int pageSize, int pageNumber)
        {
            var entity = _dbSet;
            var response = new PageList<TEntity>(entity, pageSize, pageNumber);
            return response;
        }

        public TEntity GetById(Guid id)
        {
            return _dbSet.Find(id);
        }

        public void Add(TEntity entity)
        {
            _dbSet.Add(entity);
            _context.SaveChanges();
        }

        public void Update(TEntity entity)
        {
            _dbSet.Update(entity);
            _context.SaveChanges();
        }

        public void Delete(Guid id)
        {
            TEntity entity = _dbSet.Find(id);
            _dbSet.Remove(entity);
        }

        public void Save()
        {
            _context.SaveChanges();
        }
    }
}