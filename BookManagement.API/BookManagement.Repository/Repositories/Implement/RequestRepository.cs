﻿using BookManagement.Models.Data;
using BookManagement.Models.Models;
using BookManagement.Repository.Repositories.Interface;

namespace BookManagement.Repository.Repositories.Implement
{
    public class RequestRepository : GenericRepository<Request>, IRequestRepository
    {
        public RequestRepository(BookDbContext context) : base(context)
        {
        }
    }
}