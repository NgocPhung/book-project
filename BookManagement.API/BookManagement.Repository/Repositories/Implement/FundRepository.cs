﻿using BookManagement.Models.Data;
using BookManagement.Models.Models;
using BookManagement.Repository.Repositories.Interface;

namespace BookManagement.Repository.Repositories.Implement
{
    public class FundRepository : GenericRepository<Funds>, IFundRepository
    {
        public FundRepository(BookDbContext context) : base(context)
        {
        }
    }
}