﻿using BookManagement.Models.Data;
using BookManagement.Models.Models;
using BookManagement.Repository.Repositories.Interface;

namespace BookManagement.Repository.Repositories.Implement
{
    public class IncomeRepository : GenericRepository<Income>, IIncomeRepository 
    {
        public IncomeRepository(BookDbContext context) : base(context)
        {
        }
    }
}